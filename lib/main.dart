import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'Pages/SplashScreen.dart';
import 'app_theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness: Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));
    return MaterialApp(
      title: 'Geeq Music',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: AppTheme.apptheme,
        accentColor: AppTheme.apptheme,
        cursorColor: AppTheme.apptheme,
        textTheme: AppTheme.textTheme,
        platform: TargetPlatform.iOS,
      ),
      home: SplashScreen(),
    );
  }
}
