import 'dart:ui';

import 'package:flutter/animation.dart';

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

class AppColors {
  static Color black = HexColor.fromHex('#000000');
  static Color white = HexColor.fromHex('#FFFFFF');
  static Color text_color = HexColor.fromHex('#3A3A3A');
  static Color text_blue_color = HexColor.fromHex('#353D52');
  static Color grey = HexColor.fromHex('#707070');
  static Color btn_color = HexColor.fromHex('#FED691');
  static Color facebook_color = HexColor.fromHex('#3C5A99');

  static Color top_color = HexColor.fromHex('#9D5CF7');
  static Color bottom_color = HexColor.fromHex('#6D78E2');
  static Color dark_blue = HexColor.fromHex('#1A203A');
  static Color back_blue = HexColor.fromHex('#1A213C');
  static Color light_blue = HexColor.fromHex('#232D4D');
  static Color grey_light = HexColor.fromHex('#A3A6B1');
  static Color blue = HexColor.fromHex('#191F38');
  static Color purple = HexColor.fromHex('#8D95FF');
  static Color light_orange = HexColor.fromHex('#FF968E');
  static Color light_yellow = HexColor.fromHex('#F9CB70');
  static Color from_date_color = HexColor.fromHex('#A8FFFF');
  static Color to_date_color = HexColor.fromHex('#FFC0FF');

  static Color top_premium = HexColor.fromHex('#23F0DE');
  static Color top_center_premium = HexColor.fromHex('#94B8FD');
  static Color bottom_center_premium = HexColor.fromHex('#CEA4F6');
  static Color bottom_premium = HexColor.fromHex('#DDAAEF');

  static Color text_grey_color = HexColor.fromHex('#A3A6B1');
  static Color play_back_color = HexColor.fromHex('#707070');

  static Color transparent = Color(0x0000000);
  static Color black_transparent = Color(0xFA00000);

}
