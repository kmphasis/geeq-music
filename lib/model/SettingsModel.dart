class SettingsModel {
  String title = '', sub_title = "";
  String image;

  SettingsModel(this.title, this.image, this.sub_title);

  factory SettingsModel.fromDocument(SettingsModel document) {
    return SettingsModel(document.title, document.image, document.sub_title);
  }
}
