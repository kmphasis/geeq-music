
import 'package:flutter/material.dart';
import 'package:geeq_music/app_theme.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

class ArtistDetailScreen extends StatefulWidget {

  @override
  _ArtistDetailScreenState createState() => _ArtistDetailScreenState();
}

class _ArtistDetailScreenState extends State<ArtistDetailScreen> {

  String isSelect = "video";

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Expanded(
                      child: Text(
                        "Arijit Singh",
                        style: TextStyle(
                            fontSize: SV.setSP(70),
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontFamily: Constants.fontName),
                      ),
                    ),
                    Image.asset(
                      ImagePath.ic_chat_round,
                      height: SV.setHeight(70),
                      width: SV.setHeight(70),
                    )
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        GestureDetector(
                          onTap:(){
                            setState(() {
                              isSelect = "video";
                            });
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                Strings.video,
                                style: TextStyle(
                                    fontSize: SV.setSP(55),
                                    color: isSelect == "video" ? Colors.white : AppColors.text_grey_color,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: Constants.fontName),
                              ),
                              Container(
                                width: SV.setWidth(70),
                                child: Divider(
                                  thickness: 3,
                                  color: isSelect == "video" ? AppColors.top_color : AppColors.transparent,
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(width: SV.setWidth(80)),
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              isSelect = "song";
                            });
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                Strings.song,
                                style: TextStyle(
                                    fontSize: SV.setSP(55),
                                    color: isSelect == "song" ? Colors.white : AppColors.text_grey_color,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: Constants.fontName),
                              ),
                              Container(
                                width: SV.setWidth(70),
                                child: Divider(
                                  thickness: 3,
                                  color: isSelect == "song" ? AppColors.top_color : AppColors.transparent,
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      height: SV.setHeight(130),
                      width: SV.setHeight(130),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: AppColors.transparent,
                          border: Border.all(color: AppColors.white,width: 1)
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.transparent,
                            image: DecorationImage(
                                image: AssetImage(ImagePath.temp_img_profile)
                            )
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: isSelect == "video" ?
                ListView.builder(
                    itemCount: 10,
                    padding: EdgeInsets.zero,
                    itemBuilder: (context,index){
                  return Container(
                    margin: EdgeInsets.fromLTRB(SV.setWidth(35),0,SV.setWidth(35),SV.setWidth(35)),
                    height: SV.setHeight(350),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 6.0)
                        ]),
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.asset(
                            ImagePath.temp_banner,
                            fit: BoxFit.fill,
                            height: double.infinity,
                            width: double.infinity,
                          ),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            height: SV.setHeight(100),
                            width: SV.setHeight(100),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.black54
                            ),
                            child: Icon(
                              Icons.play_arrow_rounded,
                              color: AppColors.white,
                              size: SV.setHeight(65),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                })
                    :ListView.builder(
                    padding:
                    EdgeInsets.symmetric(horizontal: SV.setHeight(40)),
                    itemCount: 20,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.only(bottom:SV.setHeight(50)),
                        child: Row(
                          children: [
                            Container(
                              width: SV.setHeight(115),
                              height: SV.setHeight(115),
                              decoration: BoxDecoration(
                                  color: AppColors.blue,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black26,
                                        offset: Offset(0.0, 2.0),
                                        blurRadius: 7.0)
                                  ]),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Image.asset(
                                  ImagePath.img_temp_2,
                                  fit: BoxFit.fill,
                                  height: SV.setHeight(115),
                                  width: SV.setHeight(115),
                                ),
                              ),
                            ),
                            SizedBox(width: SV.setHeight(35)),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Billie Jean',
                                    style: TextStyle(
                                        fontSize: SV.setSP(40),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: Constants.fontName),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  SizedBox(height: SV.setHeight(12)),
                                  Text(
                                    'Michael Jackson',
                                    style: TextStyle(
                                        fontSize: SV.setSP(35),
                                        color: AppColors.text_grey_color,
                                        fontFamily: Constants.fontName),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  SizedBox(height: SV.setHeight(12)),
                                  Divider(
                                    height: 2,
                                    color: AppColors.text_grey_color,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );

  }


}