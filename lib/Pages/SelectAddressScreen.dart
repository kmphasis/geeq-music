
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/AddAddressScreen.dart';
import 'package:geeq_music/Pages/PaymentScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class SelectAddressScreen extends StatefulWidget {

  @override
  _SelectAddressScreenState createState() => _SelectAddressScreenState();
}

class _SelectAddressScreenState extends State<SelectAddressScreen> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      Strings.select_address,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                    itemCount: 3,
                    itemBuilder: (context,index){
                  return GestureDetector(
                    onTap: (){
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) =>
                              PaymentScreen(),
                        ),
                      );
                    },
                    child: Container(
                      padding: EdgeInsets.all(SV.setHeight(25)),
                      margin: EdgeInsets.only(bottom:SV.setHeight(50)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                Strings.name_,
                                style: TextStyle(
                                    fontSize: SV.setSP(40),
                                    color: AppColors.white,
                                    height: 1.5,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Constants.fontName),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                              Expanded(
                                child: Text(
                                  'Arijit Singh',
                                  style: TextStyle(
                                      fontSize: SV.setSP(40),
                                      color: AppColors.white,
                                      height: 1.5,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: Constants.fontName),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: SV.setHeight(15)),
                          Row(
                            children: [
                              Text(
                                Strings.mobileno_,
                                style: TextStyle(
                                    fontSize: SV.setSP(40),
                                    color: AppColors.white,
                                    height: 1.5,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Constants.fontName),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                              Text(
                                '96541 36541',
                                style: TextStyle(
                                    fontSize: SV.setSP(40),
                                    color: AppColors.white,
                                    height: 1.5,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: Constants.fontName),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              )
                            ],
                          ),
                          SizedBox(height: SV.setHeight(15)),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                Strings.address_,
                                style: TextStyle(
                                    fontSize: SV.setSP(40),
                                    color: AppColors.white,
                                    height: 1.5,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Constants.fontName),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                              Expanded(
                                child: Text(
                                  'Akshya Nagar 1st Block 1st Crossroad',
                                  style: TextStyle(
                                      fontSize: SV.setSP(40),
                                      color: AppColors.white,
                                      height: 1.5,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: Constants.fontName),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: SV.setHeight(15)),
                          Row(
                            children: [
                              Expanded(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      Strings.city_,
                                      style: TextStyle(
                                          fontSize: SV.setSP(40),
                                          color: AppColors.white,
                                          height: 1.5,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: Constants.fontName),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Expanded(
                                      child: Text(
                                        'Surat',
                                        style: TextStyle(
                                            fontSize: SV.setSP(40),
                                            color: AppColors.white,
                                            height: 1.5,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: Constants.fontName),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      Strings.pincode_,
                                      style: TextStyle(
                                          fontSize: SV.setSP(40),
                                          color: AppColors.white,
                                          height: 1.5,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: Constants.fontName),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Expanded(
                                      child: Text(
                                        '395010',
                                        style: TextStyle(
                                            fontSize: SV.setSP(40),
                                            color: AppColors.white,
                                            height: 1.5,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: Constants.fontName),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: SV.setHeight(15)),
                          Row(
                            children: [
                              Expanded(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      Strings.state_,
                                      style: TextStyle(
                                          fontSize: SV.setSP(40),
                                          color: AppColors.white,
                                          height: 1.5,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: Constants.fontName),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Text(
                                      'Gujarat',
                                      style: TextStyle(
                                          fontSize: SV.setSP(40),
                                          color: AppColors.white,
                                          height: 1.5,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: Constants.fontName),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      Strings.country_,
                                      style: TextStyle(
                                          fontSize: SV.setSP(40),
                                          color: AppColors.white,
                                          height: 1.5,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: Constants.fontName),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Text(
                                      'India',
                                      style: TextStyle(
                                          fontSize: SV.setSP(40),
                                          color: AppColors.white,
                                          height: 1.5,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: Constants.fontName),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              ),
              SizedBox(height: SV.setHeight(80)),
              Center(
                child: GestureDetector(
                  onTap: (){
                    Navigator.push<dynamic>(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (BuildContext context) =>
                            AddAddressScreen(),
                      ),
                    );
                  },
                  child: Container(
                    height: SV.setHeight(100),
                    width: SV.setWidth(700),
                    decoration: BoxDecoration(
                      color: AppTheme.apptheme,
                      borderRadius: BorderRadius.circular(360.0),
                      gradient: new LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          AppColors.top_color,
                          AppColors.bottom_color
                        ],
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          offset: Offset(0.0, 2.0),
                          blurRadius: 3.0,
                        )
                      ],
                    ),
                    child: Center(
                      child: Text(Strings.add_new_address.toUpperCase(),
                          style: TextStyle(
                              fontSize: SV.setSP(45),
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontFamily: Constants.fontName)),
                    ),
                  ),
                ),
              ),
              SizedBox(height: SV.setHeight(80)),
            ],
          ),
        ),
      ),
    );

  }


}