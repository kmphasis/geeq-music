import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

import '../../app_theme.dart';

class BookingFilterScreen extends StatefulWidget {
  @override
  _BookingFilterScreenState createState() => _BookingFilterScreenState();
}

class _BookingFilterScreenState extends State<BookingFilterScreen> {

  RangeValues values = RangeValues(150, 400);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                  height: MediaQuery.of(context).padding.top + SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      Strings.filters,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(30)),
                      padding:
                          EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              color: AppColors.text_grey_color, width: 1)),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.artist_name,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding:
                          EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              color: AppColors.text_grey_color, width: 1)),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        enabled: false,
                        onChanged: (val) {},
                        decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.date,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.all(SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              color: AppColors.text_grey_color, width: 1)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            Strings.price_range,
                            style: TextStyle(
                                letterSpacing: 1,
                                fontSize: SV.setSP(40),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: Constants.fontName),
                          ),
                          SizedBox(height: SV.setHeight(80)),
                          RangeSlider(
                              activeColor: AppTheme.apptheme,
                              inactiveColor: Colors.blue.withOpacity(0.5),
                              min: 0,
                              max: 500,
                              divisions: 500,
                              values: values,
                              labels: RangeLabels(
                                values.start.round().toString() + "\$",
                                values.end.round().toString() + "\$",
                              ),
                              onChangeStart: (val) {},
                              onChangeEnd: (val) {},
                              onChanged: (value) {
                                print(
                                    "START: ${value.start}, End: ${value.end}");
                                setState(() {
                                  values = value;
                                });
                              }),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding:
                      EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                              color: AppColors.text_grey_color, width: 1)),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        decoration: InputDecoration(
                          contentPadding:
                          EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.location,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Center(
                      child: Container(
                        margin: EdgeInsets.only(top:SV.setHeight(150)),
                        height: SV.setHeight(100),
                        width: SV.setWidth(500),
                        decoration: BoxDecoration(
                          color: AppTheme.apptheme,
                          borderRadius: BorderRadius.circular(360.0),
                          gradient: new LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              AppColors.top_color,
                              AppColors.bottom_color
                            ],
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 3.0,
                            )
                          ],
                        ),
                        child: Center(
                          child: Text(Strings.apply_filter.toUpperCase(),
                              style: TextStyle(
                                  fontSize: SV.setSP(45),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: Constants.fontName)),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
