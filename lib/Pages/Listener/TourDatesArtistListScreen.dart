
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/Listener/TourDatesDetailScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../../app_theme.dart';

class TourDatesArtistListScreen extends StatefulWidget {
  @override
  _TourDatesArtistListScreenState createState() => _TourDatesArtistListScreenState();
}

class _TourDatesArtistListScreenState extends State<TourDatesArtistListScreen> {

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        backgroundColor: AppColors.back_blue,
        body: Padding(
          padding: EdgeInsets.only(top: SV.setHeight(50)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: SV.setHeight(30)),
              Container(
                margin: EdgeInsets.symmetric(horizontal:SV.setHeight(50)),
                decoration: BoxDecoration(
                  color: AppColors.light_blue,
                  borderRadius: BorderRadius.circular(SV.setHeight(360)),
                ),
                child: TextField(
                  style: TextStyle(
                      fontSize: SV.setSP(45),
                      color: AppTheme.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: AppTheme.fontName),
                  cursorColor: AppTheme.apptheme,
                  onChanged: (val) {

                  },
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 15),
                      border: InputBorder.none,
                      hintText: Strings.search,
                      hintStyle: TextStyle(
                          fontSize: SV.setSP(45),
                          color: AppColors.grey_light,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTheme.fontName),
                      prefixIcon: Icon(
                        Icons.search,
                        color: AppColors.grey_light,
                      )),
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: GridView.builder(
                    padding: EdgeInsets.all(0),
                    itemCount: 12,
                    gridDelegate:
                    SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        childAspectRatio: MediaQuery.of(context)
                            .size
                            .width /
                            (MediaQuery.of(context).size.height /
                                1.7)), itemBuilder: (context,index){
                  return GestureDetector(
                    onTap: (){
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) =>
                              TourDatesDetailScreen(),
                        ),
                      );
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height:SV.setHeight(230),
                          width:SV.setHeight(230),
                          margin: EdgeInsets.all(SV.setWidth(25)),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(360),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0.0, 2.0),
                                    blurRadius: 6.0)
                              ]),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(360),
                            child: Image.asset(
                              ImagePath.ic_music_back,
                              fit: BoxFit.fill,
                              height: double.infinity,
                              width: double.infinity,
                            ),
                          ),
                        ),
                        Text(
                          'Arijit Singh',
                          textAlign:TextAlign.center,
                          style: TextStyle(
                              fontSize: SV.setSP(41),
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontFamily: Constants.fontName),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        )
                      ],
                    ),
                  );
                }),
              )
            ],
          ),
        ),
      );
  }


}