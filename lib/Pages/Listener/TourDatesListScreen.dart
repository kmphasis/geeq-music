
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../../app_theme.dart';

class TourDatesListScreen extends StatefulWidget {

  @override
  _TourDatesListScreenState createState() => _TourDatesListScreenState();
}

class _TourDatesListScreenState extends State<TourDatesListScreen> {


  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Expanded(
                      child: Text(
                        Strings.tour_dates,
                        style: TextStyle(
                            fontSize: SV.setSP(70),
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontFamily: Constants.fontName),
                      ),
                    ),
                    PopupMenuButton(
                      child: Image.asset(
                        ImagePath.ic_tour_list_filter,
                        height: SV.setHeight(70),
                        width: SV.setHeight(70),
                      ),
                      onSelected: (val){
                        print(val);
                      },
                      itemBuilder: (context) {
                        return List.generate(4, (index) {
                          return PopupMenuItem(
                            child: Text(
                              index == 0 ? Strings.all : index == 1 ?Strings.pending:
                              index == 2 ? Strings.accepted : Strings.rejected,
                              style: TextStyle(
                                  fontSize: SV.setSP(40),
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: Constants.fontName),
                            ),
                          );
                        });
                      },

                    )
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: SV.setWidth(45)),
                    itemCount: 8,
                    itemBuilder: (context,index){
                  return Container(
                    padding: EdgeInsets.all(SV.setHeight(25)),
                    margin: EdgeInsets.only(bottom:SV.setHeight(45)),
                    decoration: BoxDecoration(
                      color: AppColors.light_blue,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      children: [
                        Container(
                          width: SV.setHeight(160),
                          height: SV.setHeight(190),
                          decoration: BoxDecoration(
                              color: AppColors.blue,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0.0, 2.0),
                                    blurRadius: 7.0)
                              ]),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.asset(
                              ImagePath.temp_img_profile_1,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        SizedBox(width: SV.setHeight(30)),
                        Expanded(
                          child: Column(
                            children: [
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                        'Angela Malone',
                                          style: TextStyle(
                                              fontSize: SV.setSP(45),
                                              color: Colors.white,
                                              fontWeight: FontWeight.w600,
                                              fontFamily: Constants.fontName),
                                        ),
                                      ),
                                    SizedBox(width: SV.setHeight(45)),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: AppTheme.apptheme,
                                        borderRadius: BorderRadius.circular(360.0),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0.0, 2.0),
                                            blurRadius: 3.0,
                                          )
                                        ],
                                      ),
                                      child: Center(
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(horizontal:SV.setWidth(20),vertical:SV.setHeight(12)),
                                          child: Text(
                                              Strings.accepted,
                                              style: TextStyle(
                                                  fontSize: SV.setSP(25),
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: Constants.fontName)),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: SV.setHeight(40)),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          width: SV.setHeight(40),
                                          height: SV.setHeight(40),
                                          decoration: BoxDecoration(
                                            color: AppTheme.apptheme,
                                            borderRadius: BorderRadius.circular(5)),
                                          child: Icon(
                                            Icons.date_range_rounded,
                                            size: SV.setHeight(25),
                                            color: AppColors.white,
                                          ),
                                        ),
                                        SizedBox(width: SV.setHeight(15)),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                Strings.from,
                                                style: TextStyle(
                                                    fontSize: SV.setSP(28),
                                                    color: AppColors.text_grey_color,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: Constants.fontName)),
                                            SizedBox(height: SV.setHeight(5)),
                                            Text(
                                                'November 10, 2021',
                                                style: TextStyle(
                                                    fontSize: SV.setSP(28),
                                                    color: AppColors.from_date_color,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: Constants.fontName))
                                          ],
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          width: SV.setHeight(40),
                                          height: SV.setHeight(40),
                                          decoration: BoxDecoration(
                                              color: AppColors.top_color,
                                              borderRadius: BorderRadius.circular(5)),
                                          child: Icon(
                                            Icons.date_range_rounded,
                                            size: SV.setHeight(25),
                                            color: AppColors.white,
                                          ),
                                        ),
                                        SizedBox(width: SV.setHeight(15)),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                Strings.to,
                                                style: TextStyle(
                                                    fontSize: SV.setSP(28),
                                                    color: AppColors.text_grey_color,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: Constants.fontName)),
                                            SizedBox(height: SV.setHeight(5)),
                                            Text(
                                                'November 21, 2021',
                                                style: TextStyle(
                                                    fontSize: SV.setSP(28),
                                                    color: AppColors.to_date_color,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: Constants.fontName))
                                          ],
                                        )
                                      ],
                                    )
                                  ],
                                )
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                }),
              )
            ],
          ),
        ),
      ),
    );

  }


}