
import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

class ListenerMyProfileScreen extends StatefulWidget {
  @override
  _ListenerMyProfileScreenState createState() => _ListenerMyProfileScreenState();
}

class _ListenerMyProfileScreenState extends State<ListenerMyProfileScreen> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: AppColors.back_blue,
      body: Padding(
        padding: EdgeInsets.only(top: SV.setHeight(50)),
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
          children: [
            SizedBox(height: SV.setHeight(45)),
            Center(
              child: Container(
                height: SV.setHeight(280),
                width: SV.setHeight(280),
                margin: EdgeInsets.all(SV.setWidth(25)),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(360),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black26,
                          offset: Offset(0.0, 2.0),
                          blurRadius: 6.0)
                    ]),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(360),
                  child: Image.asset(
                    ImagePath.ic_profile_pic,
                    fit: BoxFit.fill,
                    height: double.infinity,
                    width: double.infinity,
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(SV.setHeight(25)),
              margin: EdgeInsets.only(top:SV.setHeight(75)),
              decoration: BoxDecoration(
                color: AppColors.light_blue,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                children: [
                  Container(
                    width: SV.setHeight(100),
                    height: SV.setHeight(100),
                    decoration: BoxDecoration(
                        color: AppColors.purple,
                        borderRadius: BorderRadius.circular(15),
                        ),
                    child: Icon(
                      Icons.email_outlined,
                      size: SV.setHeight(60),
                      color: AppColors.white,
                    ),
                  ),
                  SizedBox(width: SV.setHeight(25)),
                  Expanded(
                    child: Text(
                      'arijit36@gmail.com',
                      style: TextStyle(
                          fontSize: SV.setSP(45),
                          color: AppColors.white,
                          height: 1.5,
                          fontWeight: FontWeight.w500,
                          fontFamily: Constants.fontName)
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(SV.setHeight(25)),
              margin: EdgeInsets.only(top:SV.setHeight(45)),
              decoration: BoxDecoration(
                color: AppColors.light_blue,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                children: [
                  Container(
                    width: SV.setHeight(100),
                    height: SV.setHeight(100),
                    decoration: BoxDecoration(
                        color: AppColors.light_orange,
                        borderRadius: BorderRadius.circular(15),
                       ),
                    child: Icon(
                      Icons.call_rounded,
                      size: SV.setHeight(60),
                      color: AppColors.white,
                    ),
                  ),
                  SizedBox(width: SV.setHeight(25)),
                  Expanded(
                    child: Text(
                        '+91 36541 XXXXX',
                        style: TextStyle(
                            fontSize: SV.setSP(45),
                            color: AppColors.white,
                            height: 1.5,
                            fontWeight: FontWeight.w500,
                            fontFamily: Constants.fontName)
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(SV.setHeight(25)),
              margin: EdgeInsets.only(top:SV.setHeight(45)),
              decoration: BoxDecoration(
                color: AppColors.light_blue,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                children: [
                  Container(
                    width: SV.setHeight(100),
                    height: SV.setHeight(100),
                    decoration: BoxDecoration(
                        color: AppColors.light_yellow,
                        borderRadius: BorderRadius.circular(15),),
                    child: Icon(
                      Icons.date_range_rounded,
                      size: SV.setHeight(60),
                      color: AppColors.white,
                    ),
                  ),
                  SizedBox(width: SV.setHeight(25)),
                  Expanded(
                    child: Text(
                        '10/12/1989',
                        style: TextStyle(
                            fontSize: SV.setSP(45),
                            color: AppColors.white,
                            height: 1.5,
                            fontWeight: FontWeight.w500,
                            fontFamily: Constants.fontName)
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

  }


}