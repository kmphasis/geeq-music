import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/AlbumSongListScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.back_blue,
      body: Padding(
        padding: EdgeInsets.only(top: SV.setHeight(50)),
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
              child: Text(
                Strings.try_something_else,
                style: TextStyle(
                    fontSize: SV.setSP(50),
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: Constants.fontName),
              ),
            ),
            SizedBox(height: SV.setHeight(50)),
            Container(
              height: SV.setHeight(390),
              child: ListView.builder(
                shrinkWrap: true,
                  itemCount: 8,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: (){
                    Navigator.push<dynamic>(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (BuildContext context) =>
                            AlbumSongListScreen(),
                      ),
                    );
                  },
                  child: Container(
                    width: SV.setHeight(300),
                    margin: index == 7 ?
                    EdgeInsets.symmetric(horizontal: SV.setHeight(45)) :
                    EdgeInsets.only(left: SV.setHeight(45)),
                    decoration: BoxDecoration(
                        color: AppColors.blue,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 7.0)
                        ]),
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image.asset(
                            ImagePath.img_temp_1,
                            fit: BoxFit.fill,
                            height: SV.setHeight(300),
                            width: SV.setHeight(300),
                          ),
                        ),
                        Expanded(
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Text(
                                'Ariana Grande, Bad Bunny, Drake. Taylor Swi',
                                maxLines:2,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: SV.setSP(28),
                                    color: AppColors.text_grey_color,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Constants.fontName,),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }),
            ),
            SizedBox(height: SV.setHeight(50)),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
              child: Text(
                Strings.shows_to_try,
                style: TextStyle(
                    fontSize: SV.setSP(50),
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: Constants.fontName),
              ),
            ),
            SizedBox(height: SV.setHeight(50)),
            Container(
              height: SV.setHeight(390),
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 8,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      width: SV.setHeight(300),
                      margin: index == 7 ?
                      EdgeInsets.symmetric(horizontal: SV.setHeight(45)) :
                      EdgeInsets.only(left: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          color: AppColors.blue,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 7.0)
                          ]),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.asset(
                              ImagePath.img_temp_2,
                              fit: BoxFit.fill,
                              height: SV.setHeight(300),
                              width: SV.setHeight(300),
                            ),
                          ),
                          Expanded(
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.all(5.0),
                                child: Text(
                                  'Ariana Grande, Bad Bunny, Drake. Taylor Swi',
                                  maxLines:2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: SV.setSP(28),
                                    color: AppColors.text_grey_color,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Constants.fontName,),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }),
            ),
            SizedBox(height: SV.setHeight(50)),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
              child: Text(
                Strings.suggested_artists,
                style: TextStyle(
                    fontSize: SV.setSP(50),
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: Constants.fontName),
              ),
            ),
            SizedBox(height: SV.setHeight(50)),
            Container(
              height: SV.setHeight(360),
              width: SV.setHeight(280),
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 8,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      width: SV.setHeight(280),
                      margin: index == 7 ?
                      EdgeInsets.symmetric(horizontal: SV.setHeight(45)) :
                      EdgeInsets.only(left: SV.setHeight(45)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: SV.setHeight(280),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(360),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 6.0)
                                ]),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(360),
                              child: Image.asset(
                                ImagePath.ic_music_back,
                                fit: BoxFit.fill,
                                width: SV.setHeight(280),
                                height: SV.setHeight(280),
                              ),
                            ),
                          ),
                          SizedBox(height: SV.setHeight(20)),
                          Text(
                            'Arijit Singh',
                            style: TextStyle(
                                fontSize: SV.setSP(41),
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontFamily: Constants.fontName),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    );
                  }),
            ),
            SizedBox(height: SV.setHeight(50)),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
              child: Text(
                Strings.popular_new_release,
                style: TextStyle(
                    fontSize: SV.setSP(50),
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: Constants.fontName),
              ),
            ),
            SizedBox(height: SV.setHeight(50)),
            Container(
              height: SV.setHeight(390),
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 8,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      width: SV.setHeight(300),
                      margin: index == 7 ?
                      EdgeInsets.symmetric(horizontal: SV.setHeight(45)) :
                      EdgeInsets.only(left: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          color: AppColors.blue,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 7.0)
                          ]),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.asset(
                              ImagePath.img_temp_3,
                              fit: BoxFit.fill,
                              height: SV.setHeight(300),
                              width: SV.setHeight(300),
                            ),
                          ),
                          Expanded(
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.all(5.0),
                                child: Text(
                                  'Ariana Grande, Bad Bunny, Drake. Taylor Swi',
                                  maxLines:2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: SV.setSP(28),
                                    color: AppColors.text_grey_color,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Constants.fontName,),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }),
            ),
            SizedBox(height: SV.setHeight(50)),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
              child: Text(
                Strings.featured_charts,
                style: TextStyle(
                    fontSize: SV.setSP(50),
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: Constants.fontName),
              ),
            ),
            SizedBox(height: SV.setHeight(50)),
            Container(
              height: SV.setHeight(390),
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 8,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      width: SV.setHeight(300),
                      margin: index == 7 ?
                      EdgeInsets.symmetric(horizontal: SV.setHeight(45)) :
                      EdgeInsets.only(left: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          color: AppColors.blue,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 7.0)
                          ]),
                      child: Column(
                        children: [
                          Container(
                            height: SV.setHeight(300),
                            width: SV.setHeight(300),
                            child: Stack(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(8),
                                  child: Image.asset(
                                    ImagePath.img_temp_4,
                                    fit: BoxFit.fill,
                                    height: SV.setHeight(300),
                                    width: SV.setHeight(300),
                                  ),
                                ),
                                Center(
                                  child: Padding(
                                    padding: EdgeInsets.all(SV.setHeight(30)),
                                    child: Text(
                                      'Top Songs Global',
                                      style: TextStyle(
                                          fontSize: SV.setSP(60),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: Constants.fontName),
                                      maxLines: 4,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.all(5.0),
                                child: Text(
                                  'Ariana Grande, Bad Bunny, Drake. Taylor Swi',
                                  maxLines:2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: SV.setSP(28),
                                    color: AppColors.text_grey_color,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Constants.fontName,),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }),
            ),
            SizedBox(height: SV.setHeight(50)),
          ],
        ),
      ),
    );
  }
}
