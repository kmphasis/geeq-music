
import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';
import 'Artist/AddBookingScreen.dart';
import 'PaymentScreen.dart';

class BookingDetailsScreen extends StatefulWidget {

  String isFrom;

  BookingDetailsScreen({this.isFrom});

  @override
  _BookingDetailsScreenState createState() => _BookingDetailsScreenState();
}

class _BookingDetailsScreenState extends State<BookingDetailsScreen> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                  height: MediaQuery.of(context).padding.top + SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      Strings.booking_detail,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(80)),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                  children: [
                    Visibility(
                      visible: Constants.userType=="Artist"?false:true,
                      child: Column(
                        children: [
                          Center(
                            child: Container(
                              width: SV.setHeight(350),
                              height: SV.setHeight(350),
                              decoration: BoxDecoration(
                                  color: AppColors.blue,
                                  borderRadius: BorderRadius.circular(360),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black26,
                                        offset: Offset(0.0, 2.0),
                                        blurRadius: 7.0)
                                  ]),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(360),
                                child: Image.asset(
                                  ImagePath.temp_img_profile_1,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: SV.setWidth(55)),
                          Center(
                            child: Text(
                              'Angela Malone',
                              style: TextStyle(
                                  fontSize: SV.setSP(55),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: Constants.fontName),
                            ),
                          ),
                          SizedBox(height: SV.setWidth(100)),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(SV.setHeight(30)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            ImagePath.ic_date_from,
                            width: SV.setHeight(80),
                            height: SV.setHeight(80),
                          ),
                          SizedBox(width: SV.setHeight(30)),
                          Expanded(
                            child: Text('November 10, 2021',
                                style: TextStyle(
                                    fontSize: SV.setSP(42),
                                    color: AppColors.white,
                                    height: 1.5,
                                    fontFamily: Constants.fontName)),
                          ),
                          Container(
                            height: SV.setHeight(60),
                            width: SV.setWidth(5),
                            color: AppColors.text_grey_color,
                          ),
                          Container(
                            width: SV.setWidth(200),
                            child: Text('\$350',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: SV.setSP(42),
                                    color: AppColors.white,
                                    height: 1.5,
                                    fontFamily: Constants.fontName)),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(SV.setHeight(30)),
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          Image.asset(
                            ImagePath.ic_text,
                            width: SV.setHeight(80),
                            height: SV.setHeight(80),
                          ),
                          SizedBox(width: SV.setHeight(30)),
                          Expanded(
                            child: Text('Concert on available city',
                                style: TextStyle(
                                    fontSize: SV.setSP(42),
                                    color: AppColors.white,
                                    height: 1.5,
                                    fontFamily: Constants.fontName)),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(SV.setHeight(30)),
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          Image.asset(
                            ImagePath.ic_location_concert,
                            width: SV.setHeight(80),
                            height: SV.setHeight(80),
                          ),
                          SizedBox(width: SV.setHeight(30)),
                          Expanded(
                            child: Text('Akshya Nagar 1st Block 1st Cross, Rammurthy.',
                                style: TextStyle(
                                    fontSize: SV.setSP(42),
                                    color: AppColors.white,
                                    height: 1.5,
                                    fontFamily: Constants.fontName)),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      height: SV.setHeight(400),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 6.0)
                          ]),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.asset(
                          ImagePath.temp_banner,
                          fit: BoxFit.fill,
                          height: double.infinity,
                          width: double.infinity,
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(50)),
                    Text(
                        Strings.description,
                        style: TextStyle(
                            fontSize: SV.setSP(40),
                            color: AppColors.white,
                            height: 1.5,
                            fontWeight: FontWeight.w500,
                            fontFamily: Constants.fontName)
                    ),
                    SizedBox(height: SV.setHeight(20)),
                    Text(
                        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                        style: TextStyle(
                            fontSize: SV.setSP(38),
                            color: AppColors.text_grey_color,
                            height: 1.5,
                            fontWeight: FontWeight.w500,
                            fontFamily: Constants.fontName)
                    ),
                    SizedBox(height: SV.setHeight(120)),
                    Visibility(
                      visible: widget.isFrom=="Booking"?Constants.userType=="Artist"?false:true:false,
                      child: Center(
                        child: GestureDetector(
                          onTap: (){
                            Navigator.push<dynamic>(
                              context,
                              MaterialPageRoute<dynamic>(
                                builder: (BuildContext context) =>
                                    PaymentScreen(),
                              ),
                            );
                          },
                          child: Container(
                            height: SV.setHeight(100),
                            width: SV.setWidth(500),
                            decoration: BoxDecoration(
                              color: AppTheme.apptheme,
                              borderRadius: BorderRadius.circular(360.0),
                              gradient: new LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  AppColors.top_color,
                                  AppColors.bottom_color
                                ],
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 3.0,
                                )
                              ],
                            ),
                            child: Center(
                              child: Text(Strings.book.toUpperCase(),
                                  style: TextStyle(
                                      fontSize: SV.setSP(45),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: Constants.fontName)),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Visibility(
                      visible: Constants.userType=="Artist"?true:false,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          GestureDetector(
                            onTap: (){
                              Navigator.push<dynamic>(
                                context,
                                MaterialPageRoute<dynamic>(
                                  builder: (BuildContext context) => AddBookingScreen(
                                    isFrom: "Edit",
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              height: SV.setHeight(100),
                              width: SV.setWidth(400),
                              decoration: BoxDecoration(
                                color: AppTheme.apptheme,
                                borderRadius: BorderRadius.circular(360.0),
                                gradient: new LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    AppColors.top_color,
                                    AppColors.bottom_color
                                  ],
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0.0, 2.0),
                                    blurRadius: 3.0,
                                  )
                                ],
                              ),
                              child: Center(
                                child: Text(Strings.edit.toUpperCase(),
                                    style: TextStyle(
                                        fontSize: SV.setSP(45),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: Constants.fontName)),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: (){

                            },
                            child: Container(
                              height: SV.setHeight(100),
                              width: SV.setWidth(400),
                              decoration: BoxDecoration(
                                color: AppTheme.apptheme,
                                borderRadius: BorderRadius.circular(360.0),
                                gradient: new LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    AppColors.top_color,
                                    AppColors.bottom_color
                                  ],
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0.0, 2.0),
                                    blurRadius: 3.0,
                                  )
                                ],
                              ),
                              child: Center(
                                child: Text(Strings.delete.toUpperCase(),
                                    style: TextStyle(
                                        fontSize: SV.setSP(45),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: Constants.fontName)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: SV.setHeight(120)),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


}