
import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class EditProfileScreen extends StatefulWidget {

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          body: Stack(
            children: [
              Image.asset(
                ImagePath.splash_bg,
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Container(
                            margin:EdgeInsets.only(top:7),
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: AppColors.white,
                              size: SV.setHeight(50),
                            ),
                          ),
                        ),
                        SizedBox(width: SV.setWidth(30)),
                        Expanded(
                          child: Text(
                            Strings.edit_profile,
                            style: TextStyle(
                                fontSize: SV.setSP(70),
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: SV.setHeight(100)),
                  Expanded(
                    child: ListView(
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      children: [
                        SizedBox(height: SV.setHeight(50)),
                        Center(
                          child: Container(
                            height: SV.setHeight(270),
                            width: SV.setHeight(230),
                            child: Stack(
                              children: [
                                Container(
                                  height: SV.setHeight(230),
                                  width: SV.setHeight(230),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: AppColors.transparent,
                                      border: Border.all(color: AppColors.white,width: 2)
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: AppColors.transparent,
                                          image: DecorationImage(
                                              image: AssetImage(ImagePath.ic_profile_pic)
                                          )
                                      ),
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Container(
                                    height: SV.setHeight(80),
                                    width: SV.setHeight(80),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: AppColors.dark_blue
                                    ),
                                    child: Icon(Icons.edit_rounded,
                                      size: SV.setHeight(40),color: AppColors.grey,),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: SV.setHeight(75)),
                        TextField(
                          style: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                          cursorColor: AppTheme.apptheme,
                          onChanged: (val) {},
                          decoration: InputDecoration(
                            hintText: Strings.name,
                            border: InputBorder.none,
                            hintStyle: TextStyle(
                                fontSize: SV.setSP(40),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                        Divider(
                          height: 2,
                          color: Colors.white,
                        ),
                        SizedBox(height: SV.setHeight(50)),
                        TextField(
                          style: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                          cursorColor: AppTheme.apptheme,
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (val) {},
                          decoration: InputDecoration(
                            hintText: Strings.email,
                            border: InputBorder.none,
                            hintStyle: TextStyle(
                                fontSize: SV.setSP(40),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                        Divider(
                          height: 2,
                          color: Colors.white,
                        ),
                        SizedBox(height: SV.setHeight(50)),
                        TextField(
                          style: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                          cursorColor: AppTheme.apptheme,
                          keyboardType: TextInputType.number,
                          onChanged: (val) {},
                          decoration: InputDecoration(
                            hintText: Strings.phone_number,
                            border: InputBorder.none,
                            hintStyle: TextStyle(
                                fontSize: SV.setSP(40),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                        Divider(
                          height: 2,
                          color: Colors.white,
                        ),
                        SizedBox(height: SV.setHeight(50)),
                        TextField(
                          style: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                          cursorColor: AppTheme.apptheme,
                          keyboardType: TextInputType.number,
                          onChanged: (val) {},
                          enabled: false,
                          decoration: InputDecoration(
                            hintText: Strings.date_of_birth,
                            border: InputBorder.none,
                            hintStyle: TextStyle(
                                fontSize: SV.setSP(40),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                        Divider(
                          height: 2,
                          color: Colors.white,
                        ),
                        Constants.userType == "Artist" ?
                        Column(
                          children: [
                            SizedBox(height: SV.setHeight(50)),
                            TextField(
                              style: TextStyle(
                                  fontSize: SV.setSP(40),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: Constants.fontName),
                              cursorColor: AppTheme.apptheme,
                              keyboardType: TextInputType.number,
                              onChanged: (val) {},
                              decoration: InputDecoration(
                                hintText: Strings.concert_price,
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    fontSize: SV.setSP(40),
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: Constants.fontName),
                              ),
                            ),
                            Divider(
                              height: 2,
                              color: Colors.white,
                            ),
                          ],
                        ) :
                        Container(),
                        SizedBox(height: SV.setHeight(80)),
                        Center(
                          child: GestureDetector(
                            onTap: (){

                            },
                            child: Container(
                              height: SV.setHeight(100),
                              width: SV.setWidth(500),
                              decoration: BoxDecoration(
                                color: AppTheme.apptheme,
                                borderRadius: BorderRadius.circular(360.0),
                                gradient: new LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    AppColors.top_color,
                                    AppColors.bottom_color
                                  ],
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0.0, 2.0),
                                    blurRadius: 3.0,
                                  )
                                ],
                              ),
                              child: Center(
                                child: Text(Strings.update.toUpperCase(),
                                    style: TextStyle(
                                        fontSize: SV.setSP(45),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: Constants.fontName)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );

  }


}