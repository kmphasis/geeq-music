
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/MainScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class PaymentSuccessScreen extends StatefulWidget {

  @override
  _PaymentSuccessScreenState createState() => _PaymentSuccessScreenState();
}

class _PaymentSuccessScreenState extends State<PaymentSuccessScreen> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  Strings.congratulations,
                  style: TextStyle(
                      fontSize: SV.setSP(70),
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: Constants.fontName),
                ),
                SizedBox(height: SV.setHeight(100)),
                Image.asset(
                  ImagePath.ic_success_payment,
                  fit: BoxFit.fill,
                  //height: SV.setHeight(700),
                  width: SV.setWidth(650),
                ),
                SizedBox(height: SV.setHeight(100)),
                Text(
                  Strings.thank_you,
                  style: TextStyle(
                      fontSize: SV.setSP(70),
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: Constants.fontName),
                ),
                SizedBox(height: SV.setHeight(100)),
                Text(
                  Strings.order_success_msg,
                  style: TextStyle(
                      fontSize: SV.setSP(42),
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: Constants.fontName),
                ),
                SizedBox(height: SV.setHeight(50)),
                Text(
                  Strings.here_order_number+"\n#125469873",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: SV.setSP(42),
                      color: Colors.white,
                      height: 1.5,
                      fontWeight: FontWeight.w500,
                      fontFamily: Constants.fontName),
                ),
                SizedBox(height: SV.setHeight(100)),
                Center(
                  child: GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (context) => MainScreen()),
                              (Route<dynamic> route) => false);
                    },
                    child: Container(
                      height: SV.setHeight(100),
                      width: SV.setWidth(500),
                      decoration: BoxDecoration(
                        color: AppTheme.apptheme,
                        borderRadius: BorderRadius.circular(360.0),
                        gradient: new LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            AppColors.top_color,
                            AppColors.bottom_color
                          ],
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            offset: Offset(0.0, 2.0),
                            blurRadius: 3.0,
                          )
                        ],
                      ),
                      child: Center(
                        child: Text(Strings.done.toUpperCase(),
                            style: TextStyle(
                                fontSize: SV.setSP(45),
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontFamily: Constants.fontName)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

  }



}