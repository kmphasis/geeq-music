import 'package:flutter/material.dart';
import 'package:geeq_music/app_theme.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import 'ChatScreen.dart';

class ChatUserListScreen extends StatefulWidget {
  @override
  _ChatUserListScreenState createState() => _ChatUserListScreenState();
}

class _ChatUserListScreenState extends State<ChatUserListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.back_blue,
      body: Column(
        children: [
          SizedBox(height: SV.setHeight(50)),
          Expanded(
            child: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                itemCount: 10,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: (){
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) => ChatScreen(),
                        ),
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.only(bottom: SV.setWidth(70)),
                      child: Row(
                        children: [
                          Container(
                            width: SV.setHeight(110),
                            height: SV.setHeight(110),
                            decoration: BoxDecoration(
                                color: AppColors.blue,
                                borderRadius: BorderRadius.circular(360),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 7.0)
                                ]),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(360),
                              child: Image.asset(
                                ImagePath.temp_img_profile_1,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          SizedBox(width: SV.setHeight(30)),
                          Expanded(
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Arijit Singh',
                                        style: TextStyle(
                                            fontSize: SV.setSP(40),
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: Constants.fontName),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    SizedBox(width: SV.setHeight(30)),
                                    Text(
                                      'Today',
                                      style: TextStyle(
                                          fontSize: SV.setSP(32),
                                          color: AppColors.text_grey_color,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: Constants.fontName),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    )
                                  ],
                                ),
                                SizedBox(height: SV.setHeight(10)),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Hi Julian! See you after work? sdf sdfs fsf dsf ',
                                        style: TextStyle(
                                            fontSize: SV.setSP(38),
                                            color: AppColors.text_grey_color,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: Constants.fontName),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    SizedBox(width: SV.setHeight(30)),
                                    Container(
                                      padding:EdgeInsets.all(7),
                                      decoration:BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: AppTheme.apptheme
                                      ),
                                      child: Text(
                                        '5',
                                        style: TextStyle(
                                            fontSize: SV.setSP(30),
                                            color: AppColors.white,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: Constants.fontName),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
