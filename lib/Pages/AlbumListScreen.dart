
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/AlbumSongListScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class AlbumListScreen extends StatefulWidget {

  @override
  _AlbumListScreenState createState() => _AlbumListScreenState();
}

class _AlbumListScreenState extends State<AlbumListScreen> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Expanded(
                      child: Text(
                        "Bollywood",
                        style: TextStyle(
                            fontSize: SV.setSP(70),
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontFamily: Constants.fontName),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(60)),
              Container(
                margin: EdgeInsets.symmetric(horizontal:SV.setHeight(50)),
                decoration: BoxDecoration(
                  color: AppColors.light_blue,
                  borderRadius: BorderRadius.circular(SV.setHeight(360)),
                ),
                child: TextField(
                  style: TextStyle(
                      fontSize: SV.setSP(45),
                      color: AppTheme.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: AppTheme.fontName),
                  cursorColor: AppTheme.apptheme,
                  onChanged: (val) {

                  },
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 15),
                      border: InputBorder.none,
                      hintText: Strings.search,
                      hintStyle: TextStyle(
                          fontSize: SV.setSP(45),
                          color: AppColors.grey_light,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTheme.fontName),
                      prefixIcon: Icon(
                        Icons.search,
                        color: AppColors.grey_light,
                      )),
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: GridView.builder(
                    padding: EdgeInsets.all(0),
                    itemCount: 12,
                    gridDelegate:
                    SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: MediaQuery.of(context)
                            .size
                            .width /
                            (MediaQuery.of(context).size.height /
                                2)), itemBuilder: (context,index){
                  return GestureDetector(
                    onTap: (){
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) =>
                              AlbumSongListScreen(),
                        ),
                      );
                    },
                   child: Container(
                     margin: EdgeInsets.all(SV.setWidth(25)),
                     decoration: BoxDecoration(
                         color: Colors.white,
                         borderRadius: BorderRadius.circular(10),
                         boxShadow: [
                           BoxShadow(
                               color: Colors.black26,
                               offset: Offset(0.0, 2.0),
                               blurRadius: 6.0)
                         ]),
                     child: Stack(
                       children: [
                         ClipRRect(
                           borderRadius: BorderRadius.circular(10),
                           child: Image.asset(
                             ImagePath.img_temp_3,
                             fit: BoxFit.fill,
                             height: double.infinity,
                           ),
                         ),
                         Align(
                           alignment: Alignment.bottomCenter,
                           child: Container(
                             width: MediaQuery.of(context).size.width,
                             child: ClipRRect(
                               borderRadius: BorderRadius.only(
                                 bottomLeft: Radius.circular(10),
                                 bottomRight: Radius.circular(10)
                               ),
                               child: BackdropFilter(
                                 filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                                 child: new Container(
                                   decoration: new BoxDecoration(
                                       color: Colors.black38,
                                     borderRadius: BorderRadius.only(
                                         bottomLeft: Radius.circular(10),
                                         bottomRight: Radius.circular(10))
                                   ),
                                   child: Padding(
                                     padding: const EdgeInsets.all(8.0),
                                     child: Text(
                                       "New Music Hindi",
                                       style: TextStyle(
                                           fontSize: SV.setSP(45),
                                           color: Colors.white,
                                           fontWeight: FontWeight.w500,
                                           fontFamily: Constants.fontName),
                                     ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                         )
                       ],
                     ),
                   ),
                  );
                }),
              )
            ],
          ),
        ),
      ),
    );

  }


}