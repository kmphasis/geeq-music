
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geeq_music/Pages/SelectArtistCategory.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

class SelectMusicCategoryScreen extends StatefulWidget {

  @override
  _SelectMusicCategoryScreenState createState() => _SelectMusicCategoryScreenState();
}

class _SelectMusicCategoryScreenState extends State<SelectMusicCategoryScreen> {

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness:
      Platform.isAndroid ? Brightness.light : Brightness.dark,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.light,
    ));

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Text(
                  Strings.what_you_like,
                  style: TextStyle(
                      fontSize: SV.setSP(70),
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontFamily: Constants.fontName),
                ),
              ),
              SizedBox(height: SV.setHeight(40)),
              Expanded(
                child: GridView.builder(
                    padding: EdgeInsets.all(0),
                    itemCount: 12,
                    gridDelegate:
                    SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: MediaQuery.of(context)
                        .size
                        .width /
                        (MediaQuery.of(context).size.height /
                            3.3)), itemBuilder: (context,index){
                  return GestureDetector(
                    onTap: (){
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) =>
                              SelectArtistCategoryScreen(),
                        ),
                      );
                    },
                    child: Container(
                      margin: EdgeInsets.all(SV.setWidth(25)),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 6.0)
                          ]),
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.asset(
                              ImagePath.ic_music_back,
                              fit: BoxFit.fill,
                              height: double.infinity,
                              width: double.infinity,
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              "English",
                              style: TextStyle(
                                  fontSize: SV.setSP(50),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: Constants.fontName),
                            ),
                          ),
                          Visibility(
                            visible:index==0||index==1?true:false,
                            child: Positioned(
                              right:5,
                              top:5,
                              child: Container(
                                height: SV.setHeight(60),
                                width: SV.setHeight(60),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: AppColors.dark_blue
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(SV.setHeight(16)),
                                  child: Image.asset(ImagePath.ic_checked),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                }),
              )
            ],
          ),
        ),
      ),
    );

  }

}