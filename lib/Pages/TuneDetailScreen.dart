
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/AddAndEditTune.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';
import 'package:seekbar/seekbar.dart';

import '../app_theme.dart';
import 'PaymentScreen.dart';

class TuneDetailsScreen extends StatefulWidget {

  String isFrom;

  TuneDetailsScreen({this.isFrom});

  @override
  _TuneDetailsScreenState createState() => _TuneDetailsScreenState();
}

class _TuneDetailsScreenState extends State<TuneDetailsScreen> {

  bool isPlay = false;

  double _value = 0.30;
  double _secondValue = 0.0;

  String isFree = "0";

  String buttonName= "";

  @override
  void initState() {
    super.initState();
    setState(() {
      if(isFree == "1" || widget.isFrom == "purchased"){
        buttonName = Strings.download;
      }else{
        buttonName = Strings.buy_now;
      }

    });
  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Padding(
            padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
            child: Column(
              children: [
                SizedBox(height: MediaQuery.of(context).padding.top + SV.setWidth(20)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      'Angela Malone',
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
                SizedBox(height: SV.setHeight(80)),
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: AppColors.blue,
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 7.0)
                            ]),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image.asset(
                            ImagePath.img_temp_2,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      SizedBox(height: SV.setHeight(60)),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Do you believe in lonelyness',
                                  style: TextStyle(
                                      fontSize: SV.setSP(45),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: Constants.fontName),
                                ),
                                SizedBox(height: SV.setHeight(12)),
                                Text(
                                  'Michael Jackson',
                                  style: TextStyle(
                                      fontSize: SV.setSP(40),
                                      color: AppColors.text_grey_color,
                                      fontFamily: Constants.fontName),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(vertical:SV.setHeight(10),horizontal: SV.setHeight(18)),
                            decoration: BoxDecoration(
                              color: isFree == "0" ? AppColors.light_orange : Colors.green,
                              borderRadius: BorderRadius.circular(360.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 3.0,
                                )
                              ],
                            ),
                            child: Center(
                              child: Text(
                                  isFree == "0" ? '\$350' : 'Free',
                                  style: TextStyle(
                                      fontSize: SV.setSP(32),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: Constants.fontName)),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: SV.setHeight(80)),
                      SeekBar(
                        value: _value,
                        secondValue: _secondValue,
                        progressColor: AppTheme.apptheme,
                        secondProgressColor: Colors.blue.withOpacity(0.5),
                        progressWidth: 3,
                        thumbRadius: 10,
                        thumbColor: AppTheme.apptheme,
                        onStartTrackingTouch: () {
                          print('onStartTrackingTouch');
                        },
                        onProgressChanged: (value) {
                          print('onProgressChanged:$value');
                          _value = value;
                        },
                        onStopTrackingTouch: () {
                          print('onStopTrackingTouch');
                        },
                      ),
                      SizedBox(height: SV.setHeight(5)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '3:24',
                            style: TextStyle(
                                fontSize: SV.setSP(33),
                                color: AppColors.text_grey_color,
                                fontFamily: Constants.fontName),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),

                          Text(
                            '4:50',
                            style: TextStyle(
                                fontSize: SV.setSP(33),
                                color: AppColors.text_grey_color,
                                fontFamily: Constants.fontName),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                      SizedBox(height: SV.setHeight(80)),
                      Center(
                        child: GestureDetector(
                          onTap: (){
                            setState(() {
                              if (isPlay) {
                                isPlay = false;
                              } else {
                                isPlay = true;
                              }
                            });
                          },
                          child: Container(
                            height: SV.setHeight(100),
                            width: SV.setHeight(100),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: AppColors.white
                            ),
                            child: Icon(
                              isPlay == true
                                  ? Icons.pause_rounded
                                  : Icons.play_arrow_rounded,
                              color: AppColors.back_blue,
                              size: SV.setHeight(65),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: SV.setHeight(80)),
                      Visibility(
                        visible: widget.isFrom=="sell"?true:false,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            GestureDetector(
                              onTap: (){
                                Navigator.push<dynamic>(
                                  context,
                                  MaterialPageRoute<dynamic>(
                                    builder: (BuildContext context) => AddAndEditTuneScreen(
                                      isFrom: "Edit",
                                    ),
                                  ),
                                );
                              },
                              child: Container(
                                height: SV.setHeight(100),
                                width: SV.setWidth(400),
                                decoration: BoxDecoration(
                                  color: AppTheme.apptheme,
                                  borderRadius: BorderRadius.circular(360.0),
                                  gradient: new LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      AppColors.top_color,
                                      AppColors.bottom_color
                                    ],
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 3.0,
                                    )
                                  ],
                                ),
                                child: Center(
                                  child: Text(Strings.edit.toUpperCase(),
                                      style: TextStyle(
                                          fontSize: SV.setSP(45),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: Constants.fontName)),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: (){

                              },
                              child: Container(
                                height: SV.setHeight(100),
                                width: SV.setWidth(400),
                                decoration: BoxDecoration(
                                  color: AppTheme.apptheme,
                                  borderRadius: BorderRadius.circular(360.0),
                                  gradient: new LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      AppColors.top_color,
                                      AppColors.bottom_color
                                    ],
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 3.0,
                                    )
                                  ],
                                ),
                                child: Center(
                                  child: Text(Strings.delete.toUpperCase(),
                                      style: TextStyle(
                                          fontSize: SV.setSP(45),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: Constants.fontName)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: widget.isFrom=="buy" || widget.isFrom=="purchased" ? true : false,
                        child: Center(
                          child: GestureDetector(
                            onTap: (){
                              if(buttonName == Strings.buy_now){
                                Navigator.push<dynamic>(
                                  context,
                                  MaterialPageRoute<dynamic>(
                                    builder: (BuildContext context) =>
                                        PaymentScreen(),
                                  ),
                                );
                              }else{

                              }
                            },
                            child: Container(
                              height: SV.setHeight(100),
                              width: SV.setWidth(500),
                              decoration: BoxDecoration(
                                color: AppTheme.apptheme,
                                borderRadius: BorderRadius.circular(360.0),
                                gradient: new LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    AppColors.top_color,
                                    AppColors.bottom_color
                                  ],
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0.0, 2.0),
                                    blurRadius: 3.0,
                                  )
                                ],
                              ),
                              child: Center(
                                child: Text(buttonName.toUpperCase(),
                                    style: TextStyle(
                                        fontSize: SV.setSP(45),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: Constants.fontName)),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );

  }

}