
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/PaymentScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class AddAddressScreen extends StatefulWidget {

  @override
  _AddAddressScreenState createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      Strings.add_new_address,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(70)),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(30)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.full_name,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.phone_number,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(
                        maxHeight: SV.setHeight(300)
                      ),
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        maxLines: null,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.enter_address,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.city,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.postal_code,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.state,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.country,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(120)),
                    Center(
                      child: GestureDetector(
                        onTap: (){
                          Navigator.push<dynamic>(
                            context,
                            MaterialPageRoute<dynamic>(
                              builder: (BuildContext context) =>
                                  PaymentScreen(),
                            ),
                          );
                        },
                        child: Container(
                          height: SV.setHeight(100),
                          width: SV.setWidth(700),
                          decoration: BoxDecoration(
                            color: AppTheme.apptheme,
                            borderRadius: BorderRadius.circular(360.0),
                            gradient: new LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                AppColors.top_color,
                                AppColors.bottom_color
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 3.0,
                              )
                            ],
                          ),
                          child: Center(
                            child: Text(Strings.continue_to_pay.toUpperCase(),
                                style: TextStyle(
                                    fontSize: SV.setSP(45),
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: Constants.fontName)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(120)),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );

  }


}