
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/AlbumListScreen.dart';
import 'package:geeq_music/Pages/ArtistListScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: AppColors.back_blue,
      body: Padding(
        padding: EdgeInsets.only(top: SV.setHeight(40)),
        child: GridView.builder(
            padding: EdgeInsets.all(0),
            itemCount: 12,
            gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: MediaQuery.of(context)
                    .size
                    .width /
                    (MediaQuery.of(context).size.height /
                        2.8)), itemBuilder: (context,index){
          return GestureDetector(
            onTap: (){
              if(index == 0){
                Navigator.push<dynamic>(
                  context,
                  MaterialPageRoute<dynamic>(
                    builder: (BuildContext context) =>
                        ArtistListScreen(),
                  ),
                );
              }else if(index==2){
                Navigator.push<dynamic>(
                  context,
                  MaterialPageRoute<dynamic>(
                    builder: (BuildContext context) =>
                        AlbumListScreen(),
                  ),
                );
              }
            },
            child: Container(
              margin: EdgeInsets.all(SV.setWidth(25)),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0.0, 2.0),
                        blurRadius: 6.0)
                  ]),
              child: Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.asset(
                      ImagePath.temp_back_1,
                      fit: BoxFit.fill,
                      height: double.infinity,
                      width: double.infinity,
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      index == 0 ? "Artists" : index == 1 ? "Listeners" : index == 2 ? "Bollywood" : "English",
                      style: TextStyle(
                          fontSize: SV.setSP(55),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );

  }


}