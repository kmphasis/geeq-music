
import 'package:flutter/material.dart';
import 'package:geeq_music/app_theme.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

class TourDatesDetailScreen extends StatefulWidget {

  String status;

  TourDatesDetailScreen({this.status});

  @override
  _TourDatesDetailScreenState createState() => _TourDatesDetailScreenState();
}

class _TourDatesDetailScreenState extends State<TourDatesDetailScreen> {

  String iseSelect = 'Home';

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      Strings.tour_dates_detail,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                  children: [
                    Center(
                      child: Container(
                        width: SV.setHeight(400),
                        height: SV.setHeight(400),
                        decoration: BoxDecoration(
                            color: AppColors.blue,
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 7.0)
                            ]),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image.asset(
                            ImagePath.temp_img_profile_1,
                            fit: BoxFit.fill,
                            height: SV.setHeight(400),
                            width: SV.setHeight(400),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(60)),
                    Text(
                      'Angela Malone',
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontFamily: Constants.fontName),
                    ),
                    SizedBox(height: SV.setHeight(75)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              ImagePath.ic_date_from,
                              width: SV.setHeight(80),
                              height: SV.setHeight(80),
                            ),
                            SizedBox(width: SV.setHeight(15)),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    Strings.from,
                                    style: TextStyle(
                                        fontSize: SV.setSP(32),
                                        color: AppColors.text_grey_color,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: Constants.fontName)),
                                SizedBox(height: SV.setHeight(5)),
                                Text(
                                    'November 10, 2021',
                                    style: TextStyle(
                                        fontSize: SV.setSP(35),
                                        color: AppColors.from_date_color,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: Constants.fontName))
                              ],
                            )
                          ],
                        ),
                        Row(
                          children: [
                          Image.asset(
                          ImagePath.ic_date_to,
                          width: SV.setHeight(80),
                          height: SV.setHeight(80),
                        ),
                            SizedBox(width: SV.setHeight(15)),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    Strings.to,
                                    style: TextStyle(
                                        fontSize: SV.setSP(32),
                                        color: AppColors.text_grey_color,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: Constants.fontName)),
                                SizedBox(height: SV.setHeight(5)),
                                Text(
                                    'November 21, 2021',
                                    style: TextStyle(
                                        fontSize: SV.setSP(35),
                                        color: AppColors.to_date_color,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: Constants.fontName))
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top:SV.setHeight(75)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(SV.setHeight(30)),
                            child:Row(
                              children: [
                                Image.asset(
                                  ImagePath.ic_home_concert,
                                  width: SV.setHeight(80),
                                  height: SV.setHeight(80),
                                ),
                                SizedBox(width: SV.setHeight(25)),
                                Expanded(
                                  child: Text(
                                      Strings.home_concert,
                                      style: TextStyle(
                                          fontSize: SV.setSP(45),
                                          color: AppColors.white,
                                          height: 1.5,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: Constants.fontName)
                                  ),
                                ),
                                Container(
                                  height: SV.setHeight(38),
                                  width: SV.setHeight(38),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: iseSelect=='Home' ? AppTheme.apptheme : AppColors.transparent,
                                    border: Border.all(width: 3,color: iseSelect=='Home' ? AppTheme.white :AppColors.text_grey_color)
                                  ),
                                )
                              ],
                            ),
                          ),
                          Divider(
                            color: AppColors.text_grey_color,
                            height: 1,
                          ),
                          Padding(
                            padding: EdgeInsets.all(SV.setHeight(30)),
                            child:Row(
                              children: [
                                Image.asset(
                                  ImagePath.ic_live_concert,
                                  width: SV.setHeight(80),
                                  height: SV.setHeight(80),
                                ),
                                SizedBox(width: SV.setHeight(25)),
                                Expanded(
                                  child: Text(
                                      Strings.live_performance,
                                      style: TextStyle(
                                          fontSize: SV.setSP(45),
                                          color: AppColors.white,
                                          height: 1.5,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: Constants.fontName)
                                  ),
                                ),
                                Container(
                                  height: SV.setHeight(38),
                                  width: SV.setHeight(38),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: iseSelect=='Live' ? AppTheme.apptheme : AppColors.transparent,
                                      border: Border.all(width: 3,color: iseSelect=='Live' ? AppTheme.white :AppColors.text_grey_color)
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top:SV.setHeight(50)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(SV.setHeight(30)),
                            child:Row(
                              children: [
                                Image.asset(
                                  ImagePath.ic_location_concert,
                                  width: SV.setHeight(80),
                                  height: SV.setHeight(80),
                                ),
                                SizedBox(width: SV.setHeight(25)),
                                Expanded(
                                  child: Text(
                                      'Akshya Nagar 1st Block 1st Cross, Rammurthy',
                                      style: TextStyle(
                                          fontSize: SV.setSP(40),
                                          color: AppColors.white,
                                          height: 1.5,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: Constants.fontName)
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: SV.setHeight(50)),
                    Text(
                        Strings.description,
                        style: TextStyle(
                            fontSize: SV.setSP(40),
                            color: AppColors.white,
                            height: 1.5,
                            fontWeight: FontWeight.w500,
                            fontFamily: Constants.fontName)
                    ),
                    SizedBox(height: SV.setHeight(20)),
                    Text(
                        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                        style: TextStyle(
                            fontSize: SV.setSP(38),
                            color: AppColors.text_grey_color,
                            height: 1.5,
                            fontWeight: FontWeight.w500,
                            fontFamily: Constants.fontName)
                    ),
                    SizedBox(height: SV.setHeight(80)),
                    widget.status == 'Pending' ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          height: SV.setHeight(100),
                          width: SV.setWidth(350),
                          decoration: BoxDecoration(
                            color: AppTheme.apptheme,
                            borderRadius: BorderRadius.circular(360.0),
                            gradient: new LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                AppColors.top_color,
                                AppColors.bottom_color
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 3.0,
                              )
                            ],
                          ),
                          child: Center(
                            child: Text(Strings.accept.toUpperCase(),
                                style: TextStyle(
                                    fontSize: SV.setSP(45),
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: Constants.fontName)),
                          ),
                        ),
                        Container(
                          height: SV.setHeight(100),
                          width: SV.setWidth(350),
                          decoration: BoxDecoration(
                            color: AppTheme.apptheme,
                            borderRadius: BorderRadius.circular(360.0),
                            gradient: new LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                AppColors.top_color,
                                AppColors.bottom_color
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 3.0,
                              )
                            ],
                          ),
                          child: Center(
                            child: Text(Strings.reject.toUpperCase(),
                                style: TextStyle(
                                    fontSize: SV.setSP(45),
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: Constants.fontName)),
                          ),
                        ),
                      ],
                    ) : Center(
                      child: Container(
                        height: SV.setHeight(100),
                        width: SV.setWidth(400),
                        decoration: BoxDecoration(
                          color: AppTheme.apptheme,
                          borderRadius: BorderRadius.circular(360.0),
                          gradient: new LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              AppColors.top_color,
                              AppColors.bottom_color
                            ],
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 3.0,
                            )
                          ],
                        ),
                        child: Center(
                          child: Text(widget.status.toUpperCase(),
                              style: TextStyle(
                                  fontSize: SV.setSP(45),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: Constants.fontName)),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(80)),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );

  }


}