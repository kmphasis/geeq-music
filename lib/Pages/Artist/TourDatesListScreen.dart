
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/Artist/TourDatesDetailScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../../app_theme.dart';

class TourDatesListScreen extends StatefulWidget {

  @override
  _TourDatesListScreenState createState() => _TourDatesListScreenState();
}

class _TourDatesListScreenState extends State<TourDatesListScreen> {

  String isSelect = "Pending";

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: AppColors.back_blue,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: SV.setHeight(50)),
          Row(
            children: [
              SizedBox(width: SV.setHeight(45)),
              GestureDetector(
                onTap: (){
                  setState(() {
                    isSelect = Strings.pending;
                  });
                },
                child: Container(
                  height: SV.setHeight(70),
                  width: SV.setWidth(260),
                  decoration: BoxDecoration(
                      color: isSelect == Strings.pending ? AppColors.light_blue: AppColors.transparent,
                      borderRadius: BorderRadius.circular(360),
                      border: Border.all(color: AppColors.white,width: 1)
                  ),
                  child:Center(
                    child: Text(
                      Strings.pending,
                      style: TextStyle(
                          fontSize: SV.setSP(35),
                          color: isSelect == Strings.pending ? Colors.white : AppColors.text_grey_color,
                          fontWeight: FontWeight.w500,
                          fontFamily: Constants.fontName),
                    ),
                  ),
                ),
              ),
              SizedBox(width: SV.setHeight(30)),
              GestureDetector(
                onTap: (){
                  setState(() {
                    isSelect = Strings.accepted;
                  });
                },
                child: Container(
                  height: SV.setHeight(70),
                  width: SV.setWidth(260),
                  decoration: BoxDecoration(
                      color: isSelect == Strings.accepted ? AppColors.light_blue: AppColors.transparent,
                      borderRadius: BorderRadius.circular(360),
                      border: Border.all(color: AppColors.white,width: 1)
                  ),
                  child:Center(
                    child: Text(
                      Strings.accepted,
                      style: TextStyle(
                          fontSize: SV.setSP(35),
                          color: isSelect == Strings.accepted ? Colors.white : AppColors.text_grey_color,
                          fontWeight: FontWeight.w500,
                          fontFamily: Constants.fontName),
                    ),
                  ),
                ),
              ),
              SizedBox(width: SV.setHeight(30)),
              GestureDetector(
                onTap: (){
                  setState(() {
                    isSelect = Strings.rejected;
                  });
                },
                child: Container(
                  height: SV.setHeight(70),
                  width: SV.setWidth(260),
                  decoration: BoxDecoration(
                      color: isSelect == Strings.rejected ? AppColors.light_blue: AppColors.transparent,
                      borderRadius: BorderRadius.circular(360),
                      border: Border.all(color: AppColors.white,width: 1)
                  ),
                  child:Center(
                    child: Text(
                      Strings.rejected,
                      style: TextStyle(
                          fontSize: SV.setSP(35),
                          color: isSelect == Strings.rejected ? Colors.white : AppColors.text_grey_color,
                          fontWeight: FontWeight.w500,
                          fontFamily: Constants.fontName),
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: SV.setHeight(50)),
          Expanded(
            child: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(45)),
                itemCount: 8,
                itemBuilder: (context,index){
                  return GestureDetector(
                    onTap: (){
                      Navigator.push<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) => TourDatesDetailScreen(
                            status: isSelect,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      padding: EdgeInsets.all(SV.setHeight(25)),
                      margin: EdgeInsets.only(bottom:SV.setHeight(45)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: SV.setHeight(160),
                            height: SV.setHeight(190),
                            decoration: BoxDecoration(
                                color: AppColors.blue,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 7.0)
                                ]),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Image.asset(
                                ImagePath.temp_img_profile_1,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          SizedBox(width: SV.setHeight(30)),
                          Expanded(
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Angela Malone',
                                        style: TextStyle(
                                            fontSize: SV.setSP(45),
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                            fontFamily: Constants.fontName),
                                      ),
                                    ),
                                    SizedBox(width: SV.setHeight(45)),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: AppTheme.apptheme,
                                        borderRadius: BorderRadius.circular(360.0),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0.0, 2.0),
                                            blurRadius: 3.0,
                                          )
                                        ],
                                      ),
                                      child: Center(
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(horizontal:SV.setWidth(20),vertical:SV.setHeight(12)),
                                          child: Text(
                                              isSelect,
                                              style: TextStyle(
                                                  fontSize: SV.setSP(25),
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: Constants.fontName)),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: SV.setHeight(40)),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          width: SV.setHeight(40),
                                          height: SV.setHeight(40),
                                          decoration: BoxDecoration(
                                              color: AppTheme.apptheme,
                                              borderRadius: BorderRadius.circular(5)),
                                          child: Icon(
                                            Icons.date_range_rounded,
                                            size: SV.setHeight(25),
                                            color: AppColors.white,
                                          ),
                                        ),
                                        SizedBox(width: SV.setHeight(15)),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                Strings.from,
                                                style: TextStyle(
                                                    fontSize: SV.setSP(28),
                                                    color: AppColors.text_grey_color,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: Constants.fontName)),
                                            SizedBox(height: SV.setHeight(5)),
                                            Text(
                                                'November 10, 2021',
                                                style: TextStyle(
                                                    fontSize: SV.setSP(28),
                                                    color: AppColors.from_date_color,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: Constants.fontName))
                                          ],
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Container(
                                          width: SV.setHeight(40),
                                          height: SV.setHeight(40),
                                          decoration: BoxDecoration(
                                              color: AppColors.top_color,
                                              borderRadius: BorderRadius.circular(5)),
                                          child: Icon(
                                            Icons.date_range_rounded,
                                            size: SV.setHeight(25),
                                            color: AppColors.white,
                                          ),
                                        ),
                                        SizedBox(width: SV.setHeight(15)),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                Strings.to,
                                                style: TextStyle(
                                                    fontSize: SV.setSP(28),
                                                    color: AppColors.text_grey_color,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: Constants.fontName)),
                                            SizedBox(height: SV.setHeight(5)),
                                            Text(
                                                'November 21, 2021',
                                                style: TextStyle(
                                                    fontSize: SV.setSP(28),
                                                    color: AppColors.to_date_color,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: Constants.fontName))
                                          ],
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );

  }


}