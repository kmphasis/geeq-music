import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../../app_theme.dart';

class AddBookingScreen extends StatefulWidget {

  String isFrom;

  AddBookingScreen({this.isFrom});

  @override
  _AddBookingScreenState createState() => _AddBookingScreenState();
}

class _AddBookingScreenState extends State<AddBookingScreen> {

  bool isImageSelect = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                  height: MediaQuery.of(context).padding.top + SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      widget.isFrom == "Add" ? Strings.add_booking : Strings.edit_booking,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(80)),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                  children: [
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          isImageSelect = true;
                        });
                      },
                      child: Container(
                        height: SV.setHeight(400),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                                color: AppColors.text_grey_color, width: 1),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 6.0)
                            ]),
                        child: Stack(
                          children: [
                            Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(Icons.upload_rounded,
                                      size: SV.setHeight(100),
                                      color: AppColors.white),
                                  SizedBox(height: SV.setHeight(10)),
                                  Text(
                                    Strings.select_image,
                                    style: TextStyle(
                                        fontSize: SV.setSP(45),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: Constants.fontName),
                                  )
                                ],
                              ),
                            ),
                            Visibility(
                              visible: isImageSelect,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Image.asset(
                                  ImagePath.temp_banner,
                                  fit: BoxFit.fill,
                                  height: double.infinity,
                                  width: double.infinity,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.title,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        enabled: false,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.date,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.price,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(
                        maxHeight: SV.setHeight(280)
                      ),
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        maxLines: null,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.address,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: AppColors.text_grey_color,width: 1)
                      ),
                      child: TextField(
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                        cursorColor: AppTheme.apptheme,
                        onChanged: (val) {},
                        maxLines: 5,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: SV.setHeight(35)),
                          border: InputBorder.none,
                          hintText: Strings.description,
                          hintStyle: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(120)),
                    Center(
                      child: GestureDetector(
                        onTap: (){

                        },
                        child: Container(
                          height: SV.setHeight(100),
                          width: SV.setWidth(500),
                          decoration: BoxDecoration(
                            color: AppTheme.apptheme,
                            borderRadius: BorderRadius.circular(360.0),
                            gradient: new LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                AppColors.top_color,
                                AppColors.bottom_color
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 3.0,
                              )
                            ],
                          ),
                          child: Center(
                            child: Text(Strings.submit.toUpperCase(),
                                style: TextStyle(
                                    fontSize: SV.setSP(45),
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: Constants.fontName)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(120)),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
