
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../../app_theme.dart';

class UploadVideoScreen extends StatefulWidget {

  @override
  _UploadVideoScreenState createState() => _UploadVideoScreenState();
}

class _UploadVideoScreenState extends State<UploadVideoScreen> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            children: [
              SizedBox(
                  height: MediaQuery.of(context).padding.top + SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      Strings.upload_video,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(ImagePath.ic_upload_video,
                        height: SV.setHeight(180),
                        width: SV.setHeight(180)),
                    SizedBox(height: SV.setHeight(80)),
                    Text(
                      Strings.upload_new_video,
                      style: TextStyle(
                          fontSize: SV.setSP(55),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                    SizedBox(height: SV.setHeight(80)),
                    Container(
                      height: SV.setHeight(100),
                      width: SV.setWidth(550),
                      decoration: BoxDecoration(
                        color: AppTheme.apptheme,
                        borderRadius: BorderRadius.circular(360.0),
                        gradient: new LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            AppColors.top_color,
                            AppColors.bottom_color
                          ],
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            offset: Offset(0.0, 2.0),
                            blurRadius: 3.0,
                          )
                        ],
                      ),
                      child: Center(
                        child: Text(Strings.upload_a_video.toUpperCase(),
                            style: TextStyle(
                                fontSize: SV.setSP(45),
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontFamily: Constants.fontName)),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );

  }



}