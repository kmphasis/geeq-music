import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            children: [
              SizedBox(
                  height: MediaQuery.of(context).padding.top + SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Container(
                      width: SV.setHeight(90),
                      height: SV.setHeight(90),
                      decoration: BoxDecoration(
                          color: AppColors.blue,
                          borderRadius: BorderRadius.circular(360),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 7.0)
                          ]),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(360),
                        child: Image.asset(
                          ImagePath.temp_img_profile_1,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Arijit Singh",
                            style: TextStyle(
                                fontSize: SV.setSP(42),
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontFamily: Constants.fontName),
                          ),
                          SizedBox(height: SV.setWidth(10)),
                          Text(
                            "Online",
                            style: TextStyle(
                                fontSize: SV.setSP(33),
                                color: AppColors.text_grey_color,
                                fontWeight: FontWeight.w500,
                                fontFamily: Constants.fontName),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  itemCount: 10,
                  //controller: _scrollController,
                  itemBuilder: (context, index) {
                    return Container(
                      child: index %2 == 0
                          ? _buildSenderRow()
                          : _buildReceiverRow(),
                    );
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: SV.setHeight(30.0)),
                decoration: BoxDecoration(
                    color: AppColors.light_blue,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 2,
                        offset: Offset(0, 0.2), // changes position of shadow
                      ),
                    ]),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: SV.setHeight(45.0)),
                        alignment: Alignment.center,
                        child: TextField(
                          maxLines: 8,
                          minLines: 1,
                          keyboardType: TextInputType.multiline,
                          onChanged: (val) {

                          },
                          //controller: _messageController,
                          style: new TextStyle(
                              fontFamily: Constants.fontName,
                              fontSize: SV.setSP(45),
                              fontWeight: FontWeight.w400,
                              color: Colors.white),
                          textAlign: TextAlign.left,
                          cursorColor: AppColors.bottom_color,
                          decoration: new InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 10),
                              hintText: Strings.type_here,
                              hintStyle: new TextStyle(
                                  fontFamily: Constants.fontName,
                                  fontSize: SV.setSP(45),
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.text_grey_color),
                              border: InputBorder.none),
                        ),
                      ),
                    ),
                    Container(
                      height: SV.setWidth(90),
                      width: SV.setWidth(90),
                      margin: EdgeInsets.only(right: SV.setWidth(60)),
                      child: InkWell(
                          onTap: () {

                          },
                          child: Image.asset(ImagePath.ic_send_message)),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _buildSenderRow() {
    return Container(
      margin: EdgeInsets.only(
          top: SV.setHeight(30),
          left: SV.setWidth(150),
          right: SV.setWidth(40),
          bottom: SV.setHeight(30)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            child: Align(
              alignment: Alignment.centerRight,
              child: Wrap(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        gradient: new LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            AppColors.top_color,
                            AppColors.bottom_color
                          ],
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            offset: Offset(0.0, 1.0),
                            blurRadius: 2.0,
                          )
                        ],
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15),
                          bottomLeft: Radius.circular(15),
                        )),
                    child: Padding(
                      padding: EdgeInsets.all(SV.setHeight(30)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('hey! nice profile by the way',
                              style: new TextStyle(
                                  fontFamily: Constants.fontName,
                                  fontSize: SV.setSP(40),
                                  fontWeight: FontWeight.w500,
                                  color: AppColors.white  ,
                                  height: 1.3)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: SV.setHeight(15)),
          Text('8:05',
              style: new TextStyle(
                  fontFamily: Constants.fontName,
                  fontSize: SV.setSP(30),
                  fontWeight: FontWeight.w500,
                  color: AppColors.white,
                  height: 1.3))
        ],
      ),
    );
  }

  _buildReceiverRow() {
    return Container(
      margin: EdgeInsets.only(
          top: SV.setHeight(30),
          right: SV.setWidth(150),
          left: SV.setWidth(40),
          bottom: SV.setHeight(30)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Wrap(
                children: [
                  Container(
                    decoration: BoxDecoration(
                       color: AppColors.light_blue,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            offset: Offset(0.0, 1.0),
                            blurRadius: 2.0,
                          )
                        ],
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                          bottomRight: Radius.circular(20),
                        )),
                    child: Padding(
                      padding: EdgeInsets.all(SV.setHeight(30)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Curabitur vulputate arcu odio, ac facilisis diam accumsan ut.',
                              style: new TextStyle(
                                  fontFamily: Constants.fontName,
                                  fontSize: SV.setSP(40),
                                  fontWeight: FontWeight.w500,
                                  color: AppColors.white,
                                  height: 1.3)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: SV.setHeight(15)),
          Text('8:05',
              style: new TextStyle(
                  fontFamily: Constants.fontName,
                  fontSize: SV.setSP(30),
                  fontWeight: FontWeight.w500,
                  color: AppColors.white,
                  height: 1.3))
        ],
      ),
    );
  }

}
