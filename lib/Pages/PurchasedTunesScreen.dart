
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/AddAndEditTune.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';
import 'TuneDetailScreen.dart';

class PurchasedTunesScreen extends StatefulWidget {

  @override
  _PurchasedTunesScreenState createState() => _PurchasedTunesScreenState();
}

class _PurchasedTunesScreenState extends State<PurchasedTunesScreen> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top + SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      Strings.purchased_tunes,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                    itemCount: 8,
                    itemBuilder: (context,index){
                      return GestureDetector(
                        onTap: (){
                          Navigator.push<dynamic>(
                            context,
                            MaterialPageRoute<dynamic>(
                              builder: (BuildContext context) => TuneDetailsScreen(
                                isFrom: "purchased",
                              ),
                            ),
                          );
                        },
                        child: Container(
                          padding: EdgeInsets.all(SV.setHeight(25)),
                          margin: EdgeInsets.only(bottom: SV.setHeight(45)),
                          decoration: BoxDecoration(
                            color: AppColors.light_blue,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Row(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    width: SV.setHeight(120),
                                    height: SV.setHeight(120),
                                    decoration: BoxDecoration(
                                        color: AppColors.blue,
                                        borderRadius: BorderRadius.circular(15),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.black26,
                                              offset: Offset(0.0, 2.0),
                                              blurRadius: 7.0)
                                        ]),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(15),
                                      child: Image.asset(
                                        ImagePath.temp_img_profile_1,
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: SV.setHeight(25))
                                ],
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Angela Malone',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: SV.setSP(43),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: Constants.fontName),
                                    ),
                                    SizedBox(height: SV.setHeight(20)),
                                    Row(
                                      children: [
                                        Container(
                                          padding: EdgeInsets.symmetric(vertical:SV.setHeight(8),horizontal: SV.setHeight(18)),
                                          decoration: BoxDecoration(
                                            color: index == 0 ? AppColors.back_blue : Colors.green,
                                            borderRadius: BorderRadius.circular(360.0),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.black26,
                                                offset: Offset(0.0, 2.0),
                                                blurRadius: 3.0,
                                              )
                                            ],
                                          ),
                                          child: Center(
                                            child: Text(
                                                index == 0 ? '\$350' : 'Free',
                                                style: TextStyle(
                                                    fontSize: SV.setSP(28),
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w600,
                                                    fontFamily: Constants.fontName)),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height: SV.setHeight(70),
                                width: SV.setHeight(70),
                                decoration: BoxDecoration(
                                  color: AppTheme.apptheme,
                                  borderRadius: BorderRadius.circular(360.0),
                                  gradient: new LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [AppColors.top_color, AppColors.bottom_color],
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 3.0,
                                    )
                                  ],
                                ),
                                child: Icon(
                                  Icons.play_arrow_rounded,
                                  color: AppColors.white,
                                  size: SV.setHeight(45),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );

  }

}