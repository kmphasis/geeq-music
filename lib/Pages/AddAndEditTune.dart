
import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class AddAndEditTuneScreen extends StatefulWidget {

  String isFrom;

  AddAndEditTuneScreen({this.isFrom});

  @override
  _AddAndEditTuneScreenState createState() => _AddAndEditTuneScreenState();
}

class _AddAndEditTuneScreenState extends State<AddAndEditTuneScreen> {

  bool isImageSelect = false;
  String isSelect = "Free";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                  height: MediaQuery.of(context).padding.top + SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      widget.isFrom == "Add" ? Strings.add_tune : Strings.edit_tune,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(80)),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                  children: [
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          isImageSelect = true;
                        });
                      },
                      child: Container(
                        height: SV.setHeight(500),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                                color: AppColors.text_grey_color, width: 1),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 6.0)
                            ]),
                        child: Stack(
                          children: [
                            Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(Icons.upload_rounded,
                                      size: SV.setHeight(100),
                                      color: AppColors.white),
                                  SizedBox(height: SV.setHeight(10)),
                                  Text(
                                    Strings.select_image,
                                    style: TextStyle(
                                        fontSize: SV.setSP(45),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: Constants.fontName),
                                  )
                                ],
                              ),
                            ),
                            Visibility(
                              visible: isImageSelect,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Image.asset(
                                  ImagePath.temp_banner,
                                  fit: BoxFit.fill,
                                  height: double.infinity,
                                  width: double.infinity,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top:SV.setHeight(75)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(SV.setHeight(30)),
                            child:Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  child: Image.asset(
                                    ImagePath.ic_text,
                                    width: SV.setHeight(80),
                                    height: SV.setHeight(80),
                                  ),
                                  margin: EdgeInsets.only(top:SV.setHeight(10)),
                                ),
                                SizedBox(width: SV.setHeight(25)),
                                Expanded(
                                  child: TextField(
                                    style: TextStyle(
                                        fontSize: SV.setSP(45),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: Constants.fontName),
                                    cursorColor: AppTheme.apptheme,
                                    onChanged: (val) {},
                                    maxLines: null,
                                    decoration: InputDecoration(
                                      hintText: Strings.enter_tune_name,
                                      border: InputBorder.none,
                                      hintStyle: TextStyle(
                                          fontSize: SV.setSP(45),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: Constants.fontName),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top:SV.setHeight(50)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                isSelect = "Free";
                              });
                            },
                            child: Padding(
                              padding: EdgeInsets.all(SV.setHeight(30)),
                              child:Row(
                                children: [
                                  Image.asset(
                                    ImagePath.ic_free_tune,
                                    width: SV.setHeight(80),
                                    height: SV.setHeight(80),
                                  ),
                                  SizedBox(width: SV.setHeight(25)),
                                  Expanded(
                                    child: Text(
                                        Strings.free,
                                        style: TextStyle(
                                            fontSize: SV.setSP(45),
                                            color: AppColors.white,
                                            height: 1.5,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: Constants.fontName)
                                    ),
                                  ),
                                  Container(
                                    height: SV.setHeight(38),
                                    width: SV.setHeight(38),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: isSelect=='Free' ? AppTheme.apptheme : AppColors.transparent,
                                        border: Border.all(width: 3,color: isSelect=='Free' ? AppTheme.white :AppColors.text_grey_color)
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Divider(
                            color: AppColors.text_grey_color,
                            height: 1,
                          ),
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                isSelect = "Paid";
                              });
                            },
                            child: Padding(
                              padding: EdgeInsets.all(SV.setHeight(30)),
                              child:Row(
                                children: [
                                  Image.asset(
                                    ImagePath.ic_tune_paid,
                                    width: SV.setHeight(80),
                                    height: SV.setHeight(80),
                                  ),
                                  SizedBox(width: SV.setHeight(25)),
                                  Expanded(
                                    child: Text(
                                        Strings.paid,
                                        style: TextStyle(
                                            fontSize: SV.setSP(45),
                                            color: AppColors.white,
                                            height: 1.5,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: Constants.fontName)
                                    ),
                                  ),
                                  Container(
                                    height: SV.setHeight(38),
                                    width: SV.setHeight(38),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: isSelect=='Paid' ? AppTheme.apptheme : AppColors.transparent,
                                        border: Border.all(width: 3,color: isSelect=='Paid' ? AppTheme.white :AppColors.text_grey_color)
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: isSelect == "Paid" ? true : false,
                      child: Container(
                        margin: EdgeInsets.only(top:SV.setHeight(50)),
                        decoration: BoxDecoration(
                          color: AppColors.light_blue,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal:SV.setHeight(45)),
                              child:Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: TextField(
                                      style: TextStyle(
                                          fontSize: SV.setSP(45),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: Constants.fontName),
                                      cursorColor: AppTheme.apptheme,
                                      onChanged: (val) {},
                                      decoration: InputDecoration(
                                        hintText: Strings.enter_price,
                                        border: InputBorder.none,
                                        hintStyle: TextStyle(
                                            fontSize: SV.setSP(45),
                                            color: Colors.white,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: Constants.fontName),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top:SV.setHeight(120)),
                      child: Image.asset(
                        ImagePath.ic_select_tune,
                        width: SV.setHeight(150),
                        height: SV.setHeight(150),
                      ),
                    ),
                    SizedBox(height:SV.setHeight(120)),
                    Center(
                      child: GestureDetector(
                        onTap: (){

                        },
                        child: Container(
                          height: SV.setHeight(100),
                          width: SV.setWidth(500),
                          decoration: BoxDecoration(
                            color: AppTheme.apptheme,
                            borderRadius: BorderRadius.circular(360.0),
                            gradient: new LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                AppColors.top_color,
                                AppColors.bottom_color
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0.0, 2.0),
                                blurRadius: 3.0,
                              )
                            ],
                          ),
                          child: Center(
                            child: Text(Strings.submit.toUpperCase(),
                                style: TextStyle(
                                    fontSize: SV.setSP(45),
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                    fontFamily: Constants.fontName)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height:SV.setHeight(100)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}