
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/MainScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class SelectArtistCategoryScreen extends StatefulWidget {

  @override
  _SelectArtistCategoryScreenState createState() => _SelectArtistCategoryScreenState();
}

class _SelectArtistCategoryScreenState extends State<SelectArtistCategoryScreen> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Expanded(
                      child: Text(
                        Strings.choose_artist_you_like,
                        style: TextStyle(
                            fontSize: SV.setSP(70),
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontFamily: Constants.fontName),
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) => MainScreen()),
                                (Route<dynamic> route) => false);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        height: SV.setHeight(65),
                        width: SV.setHeight(65),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                          gradient: new LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              AppColors.top_color,
                              AppColors.bottom_color
                            ],
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(SV.setHeight(16)),
                          child: Image.asset(ImagePath.ic_checked),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(60)),
              Container(
                margin: EdgeInsets.symmetric(horizontal:SV.setHeight(50)),
                decoration: BoxDecoration(
                    color: AppColors.light_blue,
                    borderRadius: BorderRadius.circular(SV.setHeight(360)),
                ),
                child: TextField(
                  style: TextStyle(
                      fontSize: SV.setSP(45),
                      color: AppTheme.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: AppTheme.fontName),
                  cursorColor: AppTheme.apptheme,
                  onChanged: (val) {

                  },
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 15),
                      border: InputBorder.none,
                      hintText: Strings.search,
                      hintStyle: TextStyle(
                          fontSize: SV.setSP(45),
                          color: AppColors.grey_light,
                          fontWeight: FontWeight.w500,
                          fontFamily: AppTheme.fontName),
                      prefixIcon: Icon(
                        Icons.search,
                        color: AppColors.grey_light,
                      )),
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: GridView.builder(
                    padding: EdgeInsets.all(0),
                    itemCount: 12,
                    gridDelegate:
                    SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        childAspectRatio: MediaQuery.of(context)
                            .size
                            .width /
                            (MediaQuery.of(context).size.height /
                                1.7)), itemBuilder: (context,index){
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height:SV.setHeight(230),
                        width:SV.setHeight(230),
                        margin: EdgeInsets.all(SV.setWidth(25)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(360),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 6.0)
                            ]),
                        child: Stack(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(360),
                              child: Image.asset(
                                ImagePath.ic_music_back,
                                fit: BoxFit.fill,
                                height: double.infinity,
                                width: double.infinity,
                              ),
                            ),
                            Visibility(
                              visible:index==0||index==1||index==2?true:false,
                              child: Align(
                                alignment: Alignment.center,
                                child: Container(
                                  height: SV.setHeight(60),
                                  width: SV.setHeight(60),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: AppColors.dark_blue
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.all(SV.setHeight(16)),
                                    child: Image.asset(ImagePath.ic_checked),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Text(
                        'Arijit Singh',
                        textAlign:TextAlign.center,
                        style: TextStyle(
                            fontSize: SV.setSP(41),
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontFamily: Constants.fontName),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      )
                    ],
                  );
                }),
              )
            ],
          ),
        ),
      ),
    );

  }


}