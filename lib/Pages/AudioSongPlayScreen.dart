
import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';
import 'package:seekbar/seekbar.dart';

import '../app_theme.dart';

class AudioSongPlayScreen extends StatefulWidget {
  @override
  _AudioSongPlayScreenState createState() => _AudioSongPlayScreenState();
}

class _AudioSongPlayScreenState extends State<AudioSongPlayScreen> {

  bool isPlay = false;

  double _value = 0.30;
  double _secondValue = 0.0;

  @override
  Widget build(BuildContext context) {

    return SafeArea(
      top:false,
      bottom: false,
      child: Scaffold(
        backgroundColor: AppColors.back_blue,
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
           children: [
             SizedBox(height: MediaQuery.of(context).padding.top + SV.setWidth(20)),
             Row(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: [
                 GestureDetector(
                   onTap: () {
                     Navigator.pop(context);
                   },
                   child: Container(
                     child: Icon(
                       Icons.arrow_back_ios,
                       color: AppColors.text_grey_color,
                       size: SV.setHeight(45),
                     ),
                   ),
                 ),
                 Expanded(
                   child: Text(
                     Strings.playing_from_playlist.toUpperCase(),
                     style: TextStyle(
                         fontSize: SV.setSP(45),
                         color: Colors.white,
                         fontWeight: FontWeight.w500,
                         fontFamily: Constants.fontName),
                     textAlign: TextAlign.center,
                   ),
                 ),
                 SizedBox(width: SV.setHeight(45))
               ],
             ),
             SizedBox(height: SV.setWidth(100)),
             Expanded(child: ListView(
               children: [
                 Container(
                   width: MediaQuery.of(context).size.width,
                   height: MediaQuery.of(context).size.width,
                   decoration: BoxDecoration(
                       color: AppColors.blue,
                       borderRadius: BorderRadius.circular(8),
                       boxShadow: [
                         BoxShadow(
                             color: Colors.black26,
                             offset: Offset(0.0, 2.0),
                             blurRadius: 7.0)
                       ]),
                   child: ClipRRect(
                     borderRadius: BorderRadius.circular(8),
                     child: Image.asset(
                       ImagePath.img_temp_2,
                       fit: BoxFit.fill,
                     ),
                   ),
                 ),
                 SizedBox(height: SV.setHeight(60)),
                 Text(
                   'Do you believe in lonelyness',
                   style: TextStyle(
                       fontSize: SV.setSP(45),
                       color: Colors.white,
                       fontWeight: FontWeight.w500,
                       fontFamily: Constants.fontName),
                   maxLines: 1,
                   overflow: TextOverflow.ellipsis,
                 ),
                 SizedBox(height: SV.setHeight(12)),
                 Text(
                   'Michael Jackson',
                   style: TextStyle(
                       fontSize: SV.setSP(40),
                       color: AppColors.text_grey_color,
                       fontFamily: Constants.fontName),
                   maxLines: 1,
                   overflow: TextOverflow.ellipsis,
                 ),
                 SizedBox(height: SV.setHeight(80)),
                 SeekBar(
                   value: _value,
                   secondValue: _secondValue,
                   progressColor: AppTheme.apptheme,
                   secondProgressColor: Colors.blue.withOpacity(0.5),
                   progressWidth: 3,
                   thumbRadius: 10,
                   thumbColor: AppTheme.apptheme,
                   onStartTrackingTouch: () {
                     print('onStartTrackingTouch');
                   },
                   onProgressChanged: (value) {
                     print('onProgressChanged:$value');
                     _value = value;
                   },
                   onStopTrackingTouch: () {
                     print('onStopTrackingTouch');
                   },
                 ),
                 SizedBox(height: SV.setHeight(5)),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: [
                     Text(
                       '3:24',
                       style: TextStyle(
                           fontSize: SV.setSP(33),
                           color: AppColors.text_grey_color,
                           fontFamily: Constants.fontName),
                       maxLines: 1,
                       overflow: TextOverflow.ellipsis,
                     ),

                     Text(
                       '4:50',
                       style: TextStyle(
                           fontSize: SV.setSP(33),
                           color: AppColors.text_grey_color,
                           fontFamily: Constants.fontName),
                       maxLines: 1,
                       overflow: TextOverflow.ellipsis,
                     ),
                   ],
                 ),
                 SizedBox(height: SV.setHeight(80)),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceAround,
                   children: [
                     Image.asset(
                       ImagePath.ic_backward,
                       height: SV.setHeight(55),
                     ),
                     GestureDetector(
                       onTap: (){
                         setState(() {
                           if (isPlay) {
                             isPlay = false;
                           } else {
                             isPlay = true;
                           }
                         });
                       },
                       child: Container(
                         height: SV.setHeight(100),
                         width: SV.setHeight(100),
                         decoration: BoxDecoration(
                             shape: BoxShape.circle,
                             color: AppColors.white
                         ),
                         child: Icon(
                           isPlay == true
                               ? Icons.pause_rounded
                               : Icons.play_arrow_rounded,
                           color: AppColors.back_blue,
                           size: SV.setHeight(65),
                         ),
                       ),
                     ),
                     Image.asset(
                       ImagePath.ic_forward,
                       height: SV.setHeight(55),
                     )
                   ],
                 ),
                 SizedBox(height: SV.setHeight(80)),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   crossAxisAlignment: CrossAxisAlignment.center,
                   children: [
                     Image.asset(
                         ImagePath.ic_playlist,
                         height: SV.setHeight(55),
                         width: SV.setWidth(65)
                     ),
                     Image.asset(
                         ImagePath.ic_shuffle,
                         height: SV.setHeight(55),
                         width: SV.setWidth(70)
                     ),
                     Image.asset(
                         ImagePath.ic_repeat,
                         width: SV.setWidth(85)
                     ),
                     Image.asset(
                         ImagePath.ic_filter_sound,
                         height: SV.setHeight(50),
                         width: SV.setWidth(55)
                     ),
                     Image.asset(
                         ImagePath.ic_like,
                         height: SV.setHeight(55),
                         width: SV.setWidth(60)
                     )
                   ],
                 ),
                 SizedBox(height: SV.setHeight(80)),
               ],
             ))
           ],
          ),
        ),
      ),
    );
  }


}