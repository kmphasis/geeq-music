import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/ForgotPasswordScreen.dart';
import 'package:geeq_music/Pages/SelectProfileScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          body: Stack(
            children: [
              Image.asset(
                ImagePath.splash_bg,
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity,
              ),
              Positioned(
                right: 0,
                child: Image.asset(
                  ImagePath.top_corner,
                  width: SV.setHeight(250),
                  height: SV.setHeight(250),
                ),
              ),
              Positioned(
                left: 0,
                bottom: 0,
                child: Image.asset(
                  ImagePath.bottom_corner,
                  height: SV.setHeight(250),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(SV.setWidth(70)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      Strings.login_now,
                      style: TextStyle(
                          fontSize: SV.setSP(75),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                    SizedBox(height: SV.setHeight(150)),
                    TextField(
                      style: TextStyle(
                          fontSize: SV.setSP(40),
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontFamily: Constants.fontName),
                      cursorColor: AppTheme.apptheme,
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (val) {},
                      decoration: InputDecoration(
                        hintText: 'Email',
                        border: InputBorder.none,
                        hintStyle: TextStyle(
                            fontSize: SV.setSP(40),
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontFamily: Constants.fontName),
                      ),
                    ),
                    Divider(
                      height: 2,
                      color: Colors.white,
                    ),
                    SizedBox(height: SV.setHeight(50)),
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            style: TextStyle(
                                fontSize: SV.setSP(40),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: Constants.fontName),
                            cursorColor: AppTheme.apptheme,
                            obscureText: true,
                            onChanged: (val) {},
                            decoration: InputDecoration(
                              hintText: 'Password',
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  fontSize: SV.setSP(40),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: Constants.fontName),
                            ),
                          ),
                        ),
                        Image.asset(
                          ImagePath.ic_password_eye,
                          width: SV.setHeight(40),
                          height: SV.setHeight(40),
                        ),
                        SizedBox(width: SV.setWidth(20)),
                      ],
                    ),
                    Divider(
                      height: 2,
                      color: Colors.white,
                    ),
                    SizedBox(height: SV.setHeight(80)),
                    Center(
                      child: GestureDetector(
                        onTap: (){
                          Navigator.push<dynamic>(
                            context,
                            MaterialPageRoute<dynamic>(
                              builder: (BuildContext context) =>
                                  ForgotPasswordScreen(),
                            ),
                          );
                        },
                        child: Text(
                          Strings.forgot_password,
                          style: TextStyle(
                              fontSize: SV.setSP(40),
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(100)),
                    Center(
                      child: Container(
                        height: SV.setHeight(100),
                        width: SV.setWidth(500),
                        decoration: BoxDecoration(
                          color: AppTheme.apptheme,
                          borderRadius: BorderRadius.circular(360.0),
                          gradient: new LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              AppColors.top_color,
                              AppColors.bottom_color
                            ],
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 3.0,
                            )
                          ],
                        ),
                        child: Center(
                          child: Text(Strings.login.toUpperCase(),
                              style: TextStyle(
                                  fontSize: SV.setSP(45),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: Constants.fontName)),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(100)),
                    Center(
                      child: GestureDetector(
                        onTap: (){
                          Navigator.push<dynamic>(
                            context,
                            MaterialPageRoute<dynamic>(
                              builder: (BuildContext context) =>
                                  SelectProfileScreen(),
                            ),
                          );
                        },
                        child: RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: Strings.dont_have_an_account,
                                style: TextStyle(
                                    fontFamily: AppTheme.fontName,
                                    fontSize: SV.setSP(40),
                                    color: AppTheme.white),
                              ),
                              TextSpan(
                                text: Strings.sign_up,
                                style: TextStyle(
                                  fontFamily: AppTheme.fontName,
                                  fontSize: SV.setSP(40),
                                  color: AppTheme.apptheme,
                                  decoration: TextDecoration.underline,),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(100)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          ImagePath.ic_social_google,
                          width: SV.setHeight(100),
                          height: SV.setHeight(100),
                        ),
                        SizedBox(width: SV.setHeight(50)),
                        Image.asset(
                          ImagePath.ic_social_facebook,
                          width: SV.setHeight(100),
                          height: SV.setHeight(100),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
