import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/SelectAddressScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class ProductDetailScreen extends StatefulWidget {
  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  List<String> imgList = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];

  int _current = 0;
  int _selectColor = 0;
  int _selectSize = 0;
  int qty = 1;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                  height: MediaQuery.of(context).padding.top + SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      Strings.detail,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: [
                    CarouselSlider(
                      items: imgList
                          .map((item) => Container(
                                child: Container(
                                  child: ClipRRect(
                                      child: Image.network(item,
                                          fit: BoxFit.cover, width: 1000.0)),
                                ),
                              ))
                          .toList(),
                      options: CarouselOptions(
                          enlargeCenterPage: false,
                          aspectRatio: MediaQuery.of(context).size.width /
                              (MediaQuery.of(context).size.height / 2.9),
                          viewportFraction: 1.0,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _current = index;
                            });
                          }),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: imgList.map((url) {
                        int index = imgList.indexOf(url);
                        return Container(
                          width: SV.setHeight(18),
                          height: SV.setHeight(18),
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 2.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _current == index
                                ? Colors.white
                                : Colors.white24,
                          ),
                        );
                      }).toList(),
                    ),
                    SizedBox(height: SV.setHeight(50)),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  'Lather Jacket',
                                  style: TextStyle(
                                      fontSize: SV.setSP(55),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: Constants.fontName),
                                ),
                              ),
                              SizedBox(width: SV.setHeight(50)),
                              Text(
                                '\$ 350',
                                style: TextStyle(
                                    fontSize: SV.setSP(55),
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Constants.fontName),
                              )
                            ],
                          ),
                          SizedBox(height: SV.setHeight(30)),
                          Text(
                              'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                              style: TextStyle(
                                  fontSize: SV.setSP(38),
                                  color: AppColors.text_grey_color,
                                  height: 1.5,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: Constants.fontName)
                          ),
                          SizedBox(height: SV.setHeight(70)),
                          Text(
                            Strings.select_colors,
                            style: TextStyle(
                                fontSize: SV.setSP(50),
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontFamily: Constants.fontName),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: SV.setHeight(30)),
                    Container(
                      height: SV.setHeight(160),
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 8,
                          shrinkWrap: true,
                          itemBuilder: (context,index){
                            return GestureDetector(
                              onTap: (){
                                setState(() {
                                  _selectColor = index;
                                });
                              },
                              child: Container(
                                width: SV.setHeight(160),
                                margin: index == 7 ?
                                EdgeInsets.symmetric(horizontal: SV.setHeight(45)) :
                                EdgeInsets.only(left: SV.setHeight(45)),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black26,
                                          offset: Offset(0.0, 2.0),
                                          blurRadius: 6.0)
                                    ]),
                                child: Stack(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Image.asset(
                                        ImagePath.temp_img_product,
                                        fit: BoxFit.fill,
                                        height: double.infinity,
                                      ),
                                    ),
                                    Visibility(
                                      visible:index == _selectColor ? true : false,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.black54,
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.all(SV.setHeight(65)),
                                          child: Image.asset(ImagePath.ic_checked),
                                        ),
                                        width: double.infinity,
                                        height: double.infinity,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          }),
                    ),
                    SizedBox(height: SV.setHeight(80)),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      child: Text(
                        Strings.select_size,
                        style: TextStyle(
                            fontSize: SV.setSP(50),
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontFamily: Constants.fontName),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(30)),
                    Container(
                      height: SV.setHeight(80),
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 4,
                          shrinkWrap: true,
                          itemBuilder: (context,index){
                            return GestureDetector(
                              onTap: (){
                                setState(() {
                                  _selectSize = index;
                                });
                              },
                              child: Container(
                                width: SV.setHeight(80),
                                margin: index == 3 ?
                                EdgeInsets.symmetric(horizontal: SV.setHeight(45)) :
                                EdgeInsets.only(left: SV.setHeight(45)),
                                child: Stack(
                                  children: [
                                    Container(
                                      height: SV.setHeight(80),
                                      width: SV.setHeight(80),
                                      child: Center(
                                        child: Text(
                                          'S',
                                          style: TextStyle(
                                              fontSize: SV.setSP(50),
                                              color: Colors.white,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: Constants.fontName),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                        color: AppColors.light_blue,
                                        borderRadius: BorderRadius.circular(10),
                                        border: Border.all(color: AppColors.text_grey_color,width: 1)
                                      ),
                                    ),
                                    Visibility(
                                      visible:index == _selectSize ? true : false,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.black54,
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.all(SV.setHeight(25)),
                                          child: Image.asset(ImagePath.ic_checked),
                                        ),
                                        width: double.infinity,
                                        height: double.infinity,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          }),
                    ),
                    SizedBox(height: SV.setHeight(80)),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    if(qty!=1){
                                      qty--;
                                    }
                                  });
                                },
                                child: Container(
                                  height: SV.setHeight(60),
                                  width: SV.setHeight(60),
                                  child: Center(
                                    child: Icon(Icons.remove,
                                    color: AppColors.white,
                                    size: SV.setHeight(35)),
                                  ),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                      color: AppColors.light_blue,
                                      border: Border.all(color: AppColors.text_grey_color,width: 1)
                                  ),
                                ),
                              ),
                              //SizedBox(width: SV.setHeight(45)),
                              Container(
                                width: SV.setHeight(150),
                                child: Center(
                                  child: Text(
                                    qty.toString(),
                                    style: TextStyle(
                                        fontSize: SV.setSP(50),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: Constants.fontName),
                                  ),
                                ),
                              ),
                              //SizedBox(width: SV.setHeight(45)),
                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    qty++;
                                  });
                                },
                                child: Container(
                                  height: SV.setHeight(60),
                                  width: SV.setHeight(60),
                                  child: Center(
                                    child: Icon(Icons.add,
                                        color: AppColors.white,
                                        size: SV.setHeight(35)),
                                  ),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: AppColors.light_blue,
                                      border: Border.all(color: AppColors.text_grey_color,width: 1)
                                  ),
                                ),
                              )
                            ],
                          ),
                          GestureDetector(
                            onTap: (){
                              Navigator.push<dynamic>(
                                context,
                                MaterialPageRoute<dynamic>(
                                  builder: (BuildContext context) =>
                                      SelectAddressScreen(),
                                ),
                              );
                            },
                            child: Container(
                              height: SV.setHeight(100),
                              width: SV.setWidth(380),
                              decoration: BoxDecoration(
                                color: AppTheme.apptheme,
                                borderRadius: BorderRadius.circular(360.0),
                                gradient: new LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    AppColors.top_color,
                                    AppColors.bottom_color
                                  ],
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0.0, 2.0),
                                    blurRadius: 3.0,
                                  )
                                ],
                              ),
                              child: Center(
                                child: Text(Strings.buy_now.toUpperCase(),
                                    style: TextStyle(
                                        fontSize: SV.setSP(45),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: Constants.fontName)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: SV.setHeight(80)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
