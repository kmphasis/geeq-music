
import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/formatters/credit_card_cvc_input_formatter.dart';
import 'package:flutter_multi_formatter/formatters/credit_card_expiration_input_formatter.dart';
import 'package:flutter_multi_formatter/formatters/credit_card_number_input_formatter.dart';
import 'package:geeq_music/Pages/PaymentSuccessScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class PaymentScreen extends StatefulWidget {

  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          body: Stack(
            children: [
              Image.asset(
                ImagePath.splash_bg,
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity,
              ),
              Column(
                children: [
                  SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Container(
                            margin:EdgeInsets.only(top:7),
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: AppColors.white,
                              size: SV.setHeight(50),
                            ),
                          ),
                        ),
                        SizedBox(width: SV.setWidth(30)),
                        Expanded(
                          child: Text(
                            Strings.payment,
                            style: TextStyle(
                                fontSize: SV.setSP(70),
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(SV.setHeight(50)),
                            decoration: BoxDecoration(
                              color: AppColors.light_blue,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Form(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: SV.setHeight(45)),
                                  Text(
                                    Strings.card_holder_name,
                                    style: TextStyle(
                                        fontSize: SV.setSP(42),
                                        color: AppColors.text_grey_color,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: Constants.fontName),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: SV.setHeight(20)),
                                    padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                      border: Border.all(color: AppColors.text_grey_color,width: 1)
                                    ),
                                    child: TextField(
                                      style: TextStyle(
                                        letterSpacing: 1,
                                          fontSize: SV.setSP(40),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: Constants.fontName),
                                      cursorColor: AppTheme.apptheme,
                                      onChanged: (val) {},
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: SV.setHeight(55)),
                                  Text(
                                    Strings.card_number,
                                    style: TextStyle(
                                        fontSize: SV.setSP(42),
                                        color: AppColors.text_grey_color,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: Constants.fontName),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: SV.setHeight(20)),
                                    padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        border: Border.all(color: AppColors.text_grey_color,width: 1)
                                    ),
                                    child: TextField(
                                      style: TextStyle(
                                          letterSpacing: 1,
                                          fontSize: SV.setSP(40),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: Constants.fontName),
                                      cursorColor: AppTheme.apptheme,
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [CreditCardNumberInputFormatter()],
                                      onChanged: (val) {},
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: SV.setHeight(55)),
                                  Text(
                                    Strings.billing_address,
                                    style: TextStyle(
                                        fontSize: SV.setSP(42),
                                        color: AppColors.text_grey_color,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: Constants.fontName),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: SV.setHeight(20)),
                                    padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                        border: Border.all(color: AppColors.text_grey_color,width: 1)
                                    ),
                                    child: TextField(
                                      style: TextStyle(
                                          letterSpacing: 1,
                                          fontSize: SV.setSP(40),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: Constants.fontName),
                                      cursorColor: AppTheme.apptheme,
                                      onChanged: (val) {},
                                      maxLines: 3,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: SV.setHeight(55)),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              Strings.exp_date,
                                              style: TextStyle(
                                                  fontSize: SV.setSP(42),
                                                  color: AppColors.text_grey_color,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: Constants.fontName),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: SV.setHeight(20)),
                                              padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(10),
                                                  border: Border.all(color: AppColors.text_grey_color,width: 1)
                                              ),
                                              child: TextField(
                                                style: TextStyle(
                                                    letterSpacing: 1,
                                                    fontSize: SV.setSP(40),
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w400,
                                                    fontFamily: Constants.fontName),
                                                cursorColor: AppTheme.apptheme,
                                                keyboardType: TextInputType.number,
                                                inputFormatters: [CreditCardExpirationDateFormatter()],
                                                onChanged: (val) {},
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: "MM/YY",
                                                  hintStyle: TextStyle(
                                                      fontSize: SV.setSP(40),
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w400,
                                                      fontFamily: Constants.fontName),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(width: SV.setHeight(45)),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              Strings.cvv,
                                              style: TextStyle(
                                                  fontSize: SV.setSP(42),
                                                  color: AppColors.text_grey_color,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: Constants.fontName),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: SV.setHeight(20)),
                                              padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(10),
                                                  border: Border.all(color: AppColors.text_grey_color,width: 1)
                                              ),
                                              child: TextField(
                                                style: TextStyle(
                                                    letterSpacing: 1,
                                                    fontSize: SV.setSP(40),
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w400,
                                                    fontFamily: Constants.fontName),
                                                cursorColor: AppTheme.apptheme,
                                                keyboardType: TextInputType.number,
                                                inputFormatters: [CreditCardCvcInputFormatter()],
                                                onChanged: (val) {},
                                                obscureText: true,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: SV.setHeight(100)),
                                  Center(
                                    child: GestureDetector(
                                      onTap: (){
                                        Navigator.push<dynamic>(
                                          context,
                                          MaterialPageRoute<dynamic>(
                                            builder: (BuildContext context) =>
                                                PaymentSuccessScreen()
                                          ),
                                        );
                                      },
                                      child: Container(
                                        height: SV.setHeight(100),
                                        width: SV.setWidth(500),
                                        decoration: BoxDecoration(
                                          color: AppTheme.apptheme,
                                          borderRadius: BorderRadius.circular(360.0),
                                          gradient: new LinearGradient(
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter,
                                            colors: [
                                              AppColors.top_color,
                                              AppColors.bottom_color
                                            ],
                                          ),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.black26,
                                              offset: Offset(0.0, 2.0),
                                              blurRadius: 3.0,
                                            )
                                          ],
                                        ),
                                        child: Center(
                                          child: Text(Strings.pay.toUpperCase(),
                                              style: TextStyle(
                                                  fontSize: SV.setSP(45),
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: Constants.fontName)),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: SV.setHeight(50)),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );

  }


}