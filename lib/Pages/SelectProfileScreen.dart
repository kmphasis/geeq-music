import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/RegisterScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class SelectProfileScreen extends StatefulWidget {
  @override
  _SelectProfileScreenState createState() => _SelectProfileScreenState();
}

class _SelectProfileScreenState extends State<SelectProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          body: Stack(
            children: [
              Image.asset(
                ImagePath.splash_bg,
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity,
              ),
              Positioned(
                right: 0,
                child: Image.asset(
                  ImagePath.top_corner,
                  width: SV.setHeight(250),
                  height: SV.setHeight(250),
                ),
              ),
              Positioned(
                left: 0,
                bottom: 0,
                child: Image.asset(
                  ImagePath.bottom_corner,
                  height: SV.setHeight(250),
                ),
              ),
              Positioned(
                  top: SV.setHeight(150),
                  left: SV.setWidth(60),
                  child: GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: AppColors.white,
                      size: SV.setHeight(45),
                    ),
                  )),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      ImagePath.ic_select_profile,
                      fit: BoxFit.fill,
                      height: SV.setHeight(700),
                    ),
                    SizedBox(height: SV.setHeight(150)),
                    GestureDetector(
                      onTap: (){
                        Constants.userType= "Listener";
                        Navigator.push<dynamic>(
                          context,
                          MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) =>
                                RegisterScreen(isFrom: "Listener"),
                          ),
                        );
                      },
                      child: Container(
                        height: SV.setHeight(100),
                        width: SV.setWidth(500),
                        decoration: BoxDecoration(
                          color: AppTheme.apptheme,
                          borderRadius: BorderRadius.circular(360.0),
                          gradient: new LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [AppColors.top_color, AppColors.bottom_color],
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 3.0,
                            )
                          ],
                        ),
                        child: Center(
                          child: Text(Strings.listner.toUpperCase(),
                              style: TextStyle(
                                  fontSize: SV.setSP(45),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: Constants.fontName)),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setHeight(60)),
                    GestureDetector(
                      onTap: (){
                        Constants.userType= "Artist";
                        Navigator.push<dynamic>(
                          context,
                          MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) =>
                                RegisterScreen(isFrom: "Artist"),
                          ),
                        );
                      },
                      child: Container(
                        height: SV.setHeight(100),
                        width: SV.setWidth(500),
                        decoration: BoxDecoration(
                          color: AppTheme.apptheme,
                          borderRadius: BorderRadius.circular(360.0),
                          gradient: new LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [AppColors.top_color, AppColors.bottom_color],
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 3.0,
                            )
                          ],
                        ),
                        child: Center(
                          child: Text(Strings.artist.toUpperCase(),
                              style: TextStyle(
                                  fontSize: SV.setSP(45),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: Constants.fontName)),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
