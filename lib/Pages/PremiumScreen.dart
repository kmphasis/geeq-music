
import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class PremiumScreen extends StatefulWidget {
  @override
  _PremiumScreenState createState() => _PremiumScreenState();
}

class _PremiumScreenState extends State<PremiumScreen> {

  @override
  Widget build(BuildContext context) {

    int isSelect = 1;

    return Scaffold(
      backgroundColor: AppColors.back_blue,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left:SV.setHeight(125),top:SV.setHeight(15)),
            child: Text('Unlock all features with premium plan',
                style: TextStyle(
                    fontFamily: Constants.fontName,
                    fontSize: SV.setSP(38),
                    fontWeight: FontWeight.w400,
                    color: AppColors.text_grey_color,
                    height: 1.3)),
          ),
          SizedBox(height: SV.setHeight(50)),
          Expanded(
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
              children: [
                Row(
                  mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      height: SV.setHeight(350),
                      width: SV.setHeight(300),
                      decoration: BoxDecoration(
                          gradient: new LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            stops: [
                              0.1,
                              0.4,
                              0.6,
                              0.9,
                            ],
                            colors: [
                              AppColors.top_premium,
                              AppColors.top_center_premium,
                              AppColors.bottom_center_premium,
                              AppColors.bottom_premium
                            ],
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 1.0),
                              blurRadius: 2.0,
                            )
                          ],
                          borderRadius: BorderRadius.circular(20)),
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: EdgeInsets.only(top: SV.setHeight(15)),
                              child: Text(Strings.premium,
                                  style: new TextStyle(
                                      fontFamily: Constants.fontName,
                                      fontSize: SV.setSP(35),
                                      fontWeight: FontWeight.w500,
                                      color: AppColors.black)),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text(Strings.ad_free_music,
                                textAlign: TextAlign.center,
                                style: new TextStyle(
                                    fontFamily: Constants.fontName,
                                    fontSize: SV.setSP(45),
                                    fontWeight: FontWeight.w600,
                                    color: AppColors.black  ,
                                    height: 1.3)),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: SV.setHeight(350),
                      width: SV.setHeight(300),
                      decoration: BoxDecoration(
                          gradient: new LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            stops: [
                              0.1,
                              0.4,
                              0.6,
                              0.9,
                            ],
                            colors: [
                              AppColors.top_premium,
                              AppColors.top_center_premium,
                              AppColors.bottom_center_premium,
                              AppColors.bottom_premium
                            ],
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 1.0),
                              blurRadius: 2.0,
                            )
                          ],
                          borderRadius: BorderRadius.circular(20)),
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: EdgeInsets.only(top: SV.setHeight(15)),
                              child: Text(Strings.premium,
                                  style: new TextStyle(
                                      fontFamily: Constants.fontName,
                                      fontSize: SV.setSP(35),
                                      fontWeight: FontWeight.w500,
                                      color: AppColors.black)),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text(Strings.offline_listening,
                                textAlign: TextAlign.center,
                                style: new TextStyle(
                                    fontFamily: Constants.fontName,
                                    fontSize: SV.setSP(45),
                                    fontWeight: FontWeight.w600,
                                    color: AppColors.black  ,
                                    height: 1.3)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: SV.setHeight(80)),
                Text(Strings.current_plan.toUpperCase(),
                    style: new TextStyle(
                        fontFamily: Constants.fontName,
                        fontSize: SV.setSP(45),
                        fontWeight: FontWeight.w600,
                        color: AppColors.white,)),
                SizedBox(height: SV.setHeight(80)),
                ListView.builder(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemCount: 4,
                    itemBuilder: (context,index){
                  return GestureDetector(
                    onTap: (){
                      setState(() {
                        isSelect = index;
                        print("AA_S -- " + isSelect.toString());
                      });
                    },
                    child: Container(
                      height: SV.setHeight(150),
                      margin: EdgeInsets.only(bottom: SV.setHeight(50)),
                      padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                      decoration: BoxDecoration(
                          gradient: index == isSelect ? LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            stops: [
                              0.1,
                              0.4,
                              0.6,
                              0.9,
                            ],
                            colors: [
                              AppColors.top_premium,
                              AppColors.top_center_premium,
                              AppColors.bottom_center_premium,
                              AppColors.bottom_premium
                            ],
                          ) : LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [
                              AppColors.light_blue,
                              AppColors.light_blue,
                            ],
                          ),
                          border: Border.all(color: index == isSelect ?
                          AppColors.transparent : AppColors.bottom_color,width: 1),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 1.0),
                              blurRadius: 2.0,
                            )
                          ],
                          borderRadius: BorderRadius.circular(15),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text('1 MONTH',
                                style: new TextStyle(
                                  fontFamily: Constants.fontName,
                                  fontSize: SV.setSP(45),
                                  fontWeight: FontWeight.w600,
                                  color: index == isSelect ? AppColors.black: AppColors.white,)),
                          ),
                          Text('\$ 99',
                              style: new TextStyle(
                                fontFamily: Constants.fontName,
                                fontSize: SV.setSP(45),
                                fontWeight: FontWeight.w600,
                                color: index == isSelect ? AppColors.black: AppColors.white,))
                        ],
                      ),
                    ),
                  );
                }),
                SizedBox(height: SV.setHeight(80)),
                Center(
                  child: Container(
                    height: SV.setHeight(100),
                    width: SV.setWidth(500),
                    decoration: BoxDecoration(
                      color: AppTheme.apptheme,
                      borderRadius: BorderRadius.circular(360.0),
                      gradient: new LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          AppColors.top_color,
                          AppColors.bottom_color
                        ],
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          offset: Offset(0.0, 2.0),
                          blurRadius: 3.0,
                        )
                      ],
                    ),
                    child: Center(
                      child: Text(Strings.purchase.toUpperCase(),
                          style: TextStyle(
                              fontSize: SV.setSP(45),
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontFamily: Constants.fontName)),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );

  }

}