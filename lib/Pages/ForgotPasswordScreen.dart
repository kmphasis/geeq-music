import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          body: Stack(
            children: [
              Image.asset(
                ImagePath.splash_bg,
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity,
              ),
              Positioned(
                right: 0,
                child: Image.asset(
                  ImagePath.top_corner,
                  width: SV.setHeight(250),
                  height: SV.setHeight(250),
                ),
              ),
              Positioned(
                left: 0,
                bottom: 0,
                child: Image.asset(
                  ImagePath.bottom_corner,
                  height: SV.setHeight(250),
                ),
              ),
              Positioned(
                  top: SV.setHeight(150),
                  left: SV.setWidth(60),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: AppColors.white,
                      size: SV.setHeight(45),
                    ),
                  )),
              Padding(
                padding: EdgeInsets.all(SV.setWidth(70)),
                child: Column(
                  children: [
                    SizedBox(height: SV.setHeight(150)),
                    Expanded(
                        child: ListView(
                          children: [
                            Text(
                              Strings.forgot_your_password,
                              style: TextStyle(
                                  fontSize: SV.setSP(75),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: Constants.fontName),
                            ),
                            SizedBox(height: SV.setHeight(15)),
                            Image.asset(
                              ImagePath.ic_forgot_password,
                              height: SV.setHeight(650),
                            ),
                            Text(
                              Strings.str_forgot_password,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                height: 1.5,
                                  fontSize: SV.setSP(40),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: Constants.fontName),
                            ),
                            SizedBox(height: SV.setHeight(75)),
                            TextField(
                              style: TextStyle(
                                  fontSize: SV.setSP(40),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: Constants.fontName),
                              cursorColor: AppTheme.apptheme,
                              keyboardType: TextInputType.emailAddress,
                              onChanged: (val) {},
                              decoration: InputDecoration(
                                hintText: 'Email',
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    fontSize: SV.setSP(40),
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: Constants.fontName),
                              ),
                            ),
                            Divider(
                              height: 2,
                              color: Colors.white,
                            ),
                            SizedBox(height: SV.setHeight(80)),
                            Center(
                              child: Container(
                                height: SV.setHeight(100),
                                width: SV.setWidth(500),
                                decoration: BoxDecoration(
                                  color: AppTheme.apptheme,
                                  borderRadius: BorderRadius.circular(360.0),
                                  gradient: new LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      AppColors.top_color,
                                      AppColors.bottom_color
                                    ],
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 3.0,
                                    )
                                  ],
                                ),
                                child: Center(
                                  child: Text(Strings.send.toUpperCase(),
                                      style: TextStyle(
                                          fontSize: SV.setSP(45),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: Constants.fontName)),
                                ),
                              ),
                            )
                          ],
                        )
                    )],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
