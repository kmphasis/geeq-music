import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/Artist/AddBookingScreen.dart';
import 'package:geeq_music/Pages/Artist/ArtistMyProfileScreen.dart';
import 'package:geeq_music/Pages/Artist/UploadVideoScreen.dart';
import 'package:geeq_music/Pages/BookedScreen.dart';
import 'package:geeq_music/Pages/BookingScreen.dart';
import 'package:geeq_music/Pages/BuyAndSellTunesScreen.dart';
import 'package:geeq_music/Pages/EditProfile.dart';
import 'package:geeq_music/Pages/HomeScreen.dart';
import 'package:geeq_music/Pages/LibraryScreen.dart';
import 'package:geeq_music/Pages/Listener/BookingFilter.dart';
import 'package:geeq_music/Pages/Listener/ListenerMyProfileScreen.dart';
import 'package:geeq_music/Pages/Artist/TourDatesListScreen.dart';
import 'package:geeq_music/Pages/PremiumScreen.dart';
import 'package:geeq_music/Pages/ProductListScreen.dart';
import 'package:geeq_music/Pages/PurchasedTunesScreen.dart';
import 'package:geeq_music/Pages/SearchScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import 'AddAndEditTune.dart';
import 'ChatUserListScreen.dart';
import 'Listener/TourDatesArtistListScreen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<String> menuNameArray = [];
  List<String> menuImageArray = [];

  String _selectedScreen = Strings.home;

  Widget tabBody = Container();

  @override
  void initState() {
    super.initState();

    menuNameArray.add(Strings.home);
    menuNameArray.add(Strings.search);
    menuNameArray.add(Strings.library);
    menuNameArray.add(Strings.my_profile);
    menuNameArray.add(Strings.tour_dates);
    menuNameArray.add(Strings.merchandise);
    menuNameArray.add(Strings.booking);
    menuNameArray.add(Strings.buy_sell_tunes);
    menuNameArray.add(Strings.chat);
    menuNameArray.add(Strings.premium);
    menuNameArray.add(Strings.logout);

    menuImageArray.add(ImagePath.ic_home);
    menuImageArray.add(ImagePath.ic_search);
    menuImageArray.add(ImagePath.ic_library);
    menuImageArray.add(ImagePath.ic_my_profile);
    menuImageArray.add(ImagePath.ic_date);
    menuImageArray.add(ImagePath.ic_tshirt);
    menuImageArray.add(ImagePath.ic_booking);
    menuImageArray.add(ImagePath.ic_buy_sell_tune);
    menuImageArray.add(ImagePath.ic_chat);
    menuImageArray.add(ImagePath.ic_premium);
    menuImageArray.add(ImagePath.ic_logout);

    setState(() {
      tabBody = HomeScreen();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          key: _scaffoldKey,
          drawer: new Drawer(
            child: Stack(
              children: [
                Container(
                  color: AppColors.back_blue,
                  width: double.infinity,
                  height: double.infinity,
                ),
                Column(
                  children: [
                    SizedBox(height: SV.setWidth(130)),
                    Center(
                      child: Container(
                        height: SV.setHeight(230),
                        width: SV.setHeight(230),
                        margin: EdgeInsets.all(SV.setWidth(25)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(360),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 6.0)
                            ]),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(360),
                          child: Image.asset(
                            ImagePath.ic_profile_pic,
                            fit: BoxFit.fill,
                            height: double.infinity,
                            width: double.infinity,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: SV.setWidth(10)),
                    Text(
                      'Arijit Singh',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: SV.setSP(42),
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontFamily: Constants.fontName),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: SV.setWidth(80)),
                    Expanded(
                      child: ListView.builder(
                          padding:
                              EdgeInsets.symmetric(horizontal: SV.setWidth(45)),
                          itemCount: menuNameArray.length,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                setState(() {
                                  _selectedScreen = menuNameArray[index];
                                });
                                Navigator.pop(context);
                                if (index == 0) {
                                  tabBody = HomeScreen();
                                } else if (index == 1) {
                                  tabBody = SearchScreen();
                                } else if (index == 2) {
                                  tabBody = LibraryScreen();
                                } else if (index == 3) {
                                  if (Constants.userType == "Artist") {
                                    tabBody = ArtistMyProfileScreen();
                                  } else {
                                    tabBody = ListenerMyProfileScreen();
                                  }
                                } else if (index == 4) {
                                  if (Constants.userType == "Artist") {
                                    tabBody = TourDatesListScreen();
                                  } else {
                                    tabBody = TourDatesArtistListScreen();
                                  }
                                } else if (index == 5) {
                                  tabBody = ProductListScreen();
                                } else if (index == 6) {
                                  tabBody = BookingScreen();
                                } else if (index == 7) {
                                  tabBody = BuyAndSellTunesScreen();
                                } else if (index == 8){
                                  tabBody = ChatUserListScreen();
                                } else if(index==9){
                                  tabBody = PremiumScreen();
                                }
                              },
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    vertical: SV.setWidth(35)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Image.asset(
                                        menuImageArray[index],
                                        width: SV.setHeight(45),
                                        height: SV.setHeight(45),
                                      ),
                                    ),
                                    SizedBox(width: SV.setWidth(45)),
                                    Expanded(
                                      child: Text(
                                        menuNameArray[index],
                                        style: TextStyle(
                                            fontSize: SV.setSP(45),
                                            color: Colors.white,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: Constants.fontName),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }),
                    )
                  ],
                )
              ],
            ),
          ),
          body: Container(
            child: Column(
              children: [
                SizedBox(
                    height:
                        MediaQuery.of(context).padding.top + SV.setWidth(20)),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          _scaffoldKey.currentState.openDrawer();
                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 6),
                          child: Image.asset(
                            ImagePath.ic_menu,
                            width: SV.setHeight(45),
                            height: SV.setHeight(45),
                          ),
                        ),
                      ),
                      SizedBox(width: SV.setWidth(45)),
                      Expanded(
                        child: Text(
                          _selectedScreen,
                          style: TextStyle(
                              fontSize: SV.setSP(65),
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontFamily: Constants.fontName),
                        ),
                      ),
                      button2(),
                      SizedBox(width: SV.setWidth(45)),
                      button1(),
                    ],
                  ),
                ),
                Expanded(child: tabBody)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget button1() {
    if (Constants.userType == "Artist") {
      if (_selectedScreen == Strings.home) {
        return GestureDetector(
          onTap: (){
            Navigator.push<dynamic>(
              context,
              MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => UploadVideoScreen(),
              ),
            );
          },
          child: Container(
            child: Image.asset(
              ImagePath.ic_upload_song,
              width: SV.setHeight(80),
              height: SV.setHeight(80),
            ),
          ),
        );
      } else if (_selectedScreen == Strings.booking) {
        return GestureDetector(
          onTap: () {
            Navigator.push<dynamic>(
              context,
              MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => AddBookingScreen(
                  isFrom: "Add",
                ),
              ),
            );
          },
          child: Container(
            child: Image.asset(
              ImagePath.ic_add_booking,
              width: SV.setHeight(65),
              height: SV.setHeight(65),
            ),
          ),
        );
      }
    } else {
      if (_selectedScreen == Strings.tour_dates) {
        return GestureDetector(
          onTap: () {
            Navigator.push<dynamic>(
              context,
              MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => TourDatesListScreen(),
              ),
            );
          },
          child: Container(
            child: Image.asset(
              ImagePath.ic_tour_date_list,
              width: SV.setHeight(80),
              height: SV.setHeight(80),
            ),
          ),
        );
      } else if (_selectedScreen == Strings.booking) {
        return GestureDetector(
          onTap: () {
            Navigator.push<dynamic>(
              context,
              MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => BookingFilterScreen(),
              ),
            );
          },
          child: Container(
            child: Image.asset(
              ImagePath.ic_book_filter,
              width: SV.setHeight(70),
              height: SV.setHeight(70),
            ),
          ),
        );
      }
    }

    if (_selectedScreen == Strings.my_profile) {
      return GestureDetector(
        onTap: () {
          Navigator.push<dynamic>(
            context,
            MaterialPageRoute<dynamic>(
              builder: (BuildContext context) => EditProfileScreen(),
            ),
          );
        },
        child: Container(
          margin: EdgeInsets.only(top: 5),
          height: SV.setHeight(70),
          width: SV.setHeight(70),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: new LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [AppColors.top_color, AppColors.bottom_color],
            ),
          ),
          child: Icon(
            Icons.edit_rounded,
            color: AppColors.white,
            size: SV.setHeight(35),
          ),
        ),
      );
    } else if (_selectedScreen == Strings.buy_sell_tunes) {
      return GestureDetector(
        onTap: () {
          Navigator.push<dynamic>(
            context,
            MaterialPageRoute<dynamic>(
              builder: (BuildContext context) => AddAndEditTuneScreen(
                isFrom: "Add",
              ),
            ),
          );
        },
        child: Container(
          child: Image.asset(
            ImagePath.ic_add_booking,
            width: SV.setHeight(70),
            height: SV.setHeight(70),
          ),
        ),
      );
    }

    return Container();
  }

  Widget button2() {
    if (Constants.userType == "Artist") {
    } else {
      if (_selectedScreen == Strings.booking) {
        return GestureDetector(
          onTap: () {
            Navigator.push<dynamic>(
              context,
              MaterialPageRoute<dynamic>(
                builder: (BuildContext context) => BookedScreen(),
              ),
            );
          },
          child: Container(
            child: Image.asset(
              ImagePath.ic_booked,
              width: SV.setHeight(70),
              height: SV.setHeight(70),
            ),
          ),
        );
      }
    }

    if (_selectedScreen == Strings.buy_sell_tunes) {
      return GestureDetector(
        onTap: () {
          Navigator.push<dynamic>(
            context,
            MaterialPageRoute<dynamic>(
              builder: (BuildContext context) => PurchasedTunesScreen(),
            ),
          );
        },
        child: Container(
          child: Image.asset(
            ImagePath.ic_purchased_tunes,
            width: SV.setHeight(70),
            height: SV.setHeight(70),
          ),
        ),
      );
    }

    return Container();
  }
}
