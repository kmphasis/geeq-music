import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/SelectMusicCategotyScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';
import 'LoginScreen.dart';

class RegisterScreen extends StatefulWidget {
  String isFrom;

  RegisterScreen({this.isFrom});

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          body: Stack(
            children: [
              Image.asset(
                ImagePath.splash_bg,
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity,
              ),
              Positioned(
                right: 0,
                child: Image.asset(
                  ImagePath.top_corner,
                  width: SV.setHeight(250),
                  height: SV.setHeight(250),
                ),
              ),
              Positioned(
                left: 0,
                bottom: 0,
                child: Image.asset(
                  ImagePath.bottom_corner,
                  height: SV.setHeight(250),
                ),
              ),
              Positioned(
                  top: SV.setHeight(150),
                  left: SV.setWidth(60),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: AppColors.white,
                      size: SV.setHeight(45),
                    ),
                  )),
              Padding(
                padding: EdgeInsets.all(SV.setWidth(70)),
                child: Column(
                  children: [
                    SizedBox(height: SV.setHeight(150)),
                    Expanded(
                      child: ListView(
                        padding: EdgeInsets.zero,
                        children: [
                          SizedBox(height: SV.setHeight(50)),
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  Strings.sign_up,
                                  style: TextStyle(
                                      fontSize: SV.setSP(75),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: Constants.fontName),
                                ),
                              ),
                              Container(
                                height: SV.setHeight(270),
                                width: SV.setHeight(230),
                                child: Stack(
                                  children: [
                                    Container(
                                      height: SV.setHeight(230),
                                      width: SV.setHeight(230),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: AppColors.transparent,
                                        border: Border.all(color: AppColors.white,width: 2)
                                      ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: AppColors.transparent,
                                              image: DecorationImage(
                                                image: AssetImage(ImagePath.ic_profile_pic)
                                              )
                                            ),
                                          ),
                                        ),
                                    ),
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Container(
                                        height: SV.setHeight(80),
                                        width: SV.setHeight(80),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: AppColors.dark_blue
                                        ),
                                        child: Icon(Icons.camera_alt_rounded,
                                        size: SV.setHeight(40),color: AppColors.grey,),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          TextField(
                            style: TextStyle(
                                fontSize: SV.setSP(40),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: Constants.fontName),
                            cursorColor: AppTheme.apptheme,
                            onChanged: (val) {},
                            decoration: InputDecoration(
                              hintText: Strings.name,
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  fontSize: SV.setSP(40),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: Constants.fontName),
                            ),
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          ),
                          SizedBox(height: SV.setHeight(50)),
                          TextField(
                            style: TextStyle(
                                fontSize: SV.setSP(40),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: Constants.fontName),
                            cursorColor: AppTheme.apptheme,
                            keyboardType: TextInputType.emailAddress,
                            onChanged: (val) {},
                            decoration: InputDecoration(
                              hintText: Strings.email,
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  fontSize: SV.setSP(40),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: Constants.fontName),
                            ),
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          ),
                          SizedBox(height: SV.setHeight(50)),
                          Row(
                            children: [
                              Expanded(
                                child: TextField(
                                  style: TextStyle(
                                      fontSize: SV.setSP(40),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: Constants.fontName),
                                  cursorColor: AppTheme.apptheme,
                                  obscureText: true,
                                  onChanged: (val) {},
                                  decoration: InputDecoration(
                                    hintText: Strings.password,
                                    border: InputBorder.none,
                                    hintStyle: TextStyle(
                                        fontSize: SV.setSP(40),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: Constants.fontName),
                                  ),
                                ),
                              ),
                              Image.asset(
                                ImagePath.ic_password_eye,
                                width: SV.setHeight(40),
                                height: SV.setHeight(40),
                              ),
                              SizedBox(width: SV.setWidth(20)),
                            ],
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          ),
                          SizedBox(height: SV.setHeight(50)),
                          Row(
                            children: [
                              Expanded(
                                child: TextField(
                                  style: TextStyle(
                                      fontSize: SV.setSP(40),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: Constants.fontName),
                                  cursorColor: AppTheme.apptheme,
                                  obscureText: true,
                                  onChanged: (val) {},
                                  decoration: InputDecoration(
                                    hintText: Strings.con_password,
                                    border: InputBorder.none,
                                    hintStyle: TextStyle(
                                        fontSize: SV.setSP(40),
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: Constants.fontName),
                                  ),
                                ),
                              ),
                              Image.asset(
                                ImagePath.ic_password_eye,
                                width: SV.setHeight(40),
                                height: SV.setHeight(40),
                              ),
                              SizedBox(width: SV.setWidth(20)),
                            ],
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          ),
                          SizedBox(height: SV.setHeight(50)),
                          TextField(
                            style: TextStyle(
                                fontSize: SV.setSP(40),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: Constants.fontName),
                            cursorColor: AppTheme.apptheme,
                            keyboardType: TextInputType.number,
                            onChanged: (val) {},
                            decoration: InputDecoration(
                              hintText: Strings.phone_number,
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  fontSize: SV.setSP(40),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: Constants.fontName),
                            ),
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          ),
                          SizedBox(height: SV.setHeight(50)),
                          TextField(
                            style: TextStyle(
                                fontSize: SV.setSP(40),
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontFamily: Constants.fontName),
                            cursorColor: AppTheme.apptheme,
                            keyboardType: TextInputType.number,
                            onChanged: (val) {},
                            enabled: false,
                            decoration: InputDecoration(
                              hintText: Strings.date_of_birth,
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  fontSize: SV.setSP(40),
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: Constants.fontName),
                            ),
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          ),
                          widget.isFrom == "Artist" ?
                          Column(
                            children: [
                              SizedBox(height: SV.setHeight(50)),
                              TextField(
                                style: TextStyle(
                                    fontSize: SV.setSP(40),
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: Constants.fontName),
                                cursorColor: AppTheme.apptheme,
                                keyboardType: TextInputType.number,
                                onChanged: (val) {},
                                decoration: InputDecoration(
                                  hintText: Strings.concert_price,
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                      fontSize: SV.setSP(40),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: Constants.fontName),
                                ),
                              ),
                              Divider(
                                height: 2,
                                color: Colors.white,
                              ),
                            ],
                          ) :
                          Container(),
                          SizedBox(height: SV.setHeight(80)),
                          Center(
                            child: GestureDetector(
                              onTap: (){
                                Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (context) => SelectMusicCategoryScreen()),
                                        (Route<dynamic> route) => false);
                              },
                              child: Container(
                                height: SV.setHeight(100),
                                width: SV.setWidth(500),
                                decoration: BoxDecoration(
                                  color: AppTheme.apptheme,
                                  borderRadius: BorderRadius.circular(360.0),
                                  gradient: new LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      AppColors.top_color,
                                      AppColors.bottom_color
                                    ],
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 3.0,
                                    )
                                  ],
                                ),
                                child: Center(
                                  child: Text(Strings.sign_up.toUpperCase(),
                                      style: TextStyle(
                                          fontSize: SV.setSP(45),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: Constants.fontName)),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: SV.setHeight(80)),
                          Center(
                            child: GestureDetector(
                              onTap: (){
                                Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (context) => LoginScreen()),
                                        (Route<dynamic> route) => false);
                              },
                              child: RichText(
                                text: TextSpan(
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: Strings.you_have_an_account,
                                      style: TextStyle(
                                          fontFamily: AppTheme.fontName,
                                          fontSize: SV.setSP(40),
                                          color: AppTheme.white),
                                    ),
                                    TextSpan(
                                      text: Strings.login,
                                      style: TextStyle(
                                        fontFamily: AppTheme.fontName,
                                        fontSize: SV.setSP(40),
                                        color: AppTheme.apptheme,
                                        decoration: TextDecoration.underline,),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
