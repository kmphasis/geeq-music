
import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import 'ArtistDetailScreen.dart';

class LibraryScreen extends StatefulWidget {
  @override
  _LibraryScreenState createState() => _LibraryScreenState();
}

class _LibraryScreenState extends State<LibraryScreen> {

  String isSelect = "music";
  String isMusicSelect = "playlist";
  String isPodcastSelect = "episode";

  ScrollController controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.back_blue,
      body: Column(
        children: [
          SizedBox(height: SV.setHeight(50)),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
            child: Column(
              children: [
                Row(
                  children: [
                    GestureDetector(
                      onTap:(){
                        setState(() {
                          isSelect = "music";
                        });
                      },
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              Strings.music,
                              style: TextStyle(
                                  fontSize: SV.setSP(55),
                                  color: isSelect == "music" ? Colors.white : AppColors.text_grey_color,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: Constants.fontName),
                            ),
                            Container(
                              width: SV.setWidth(70),
                              child: Divider(
                                thickness: 3,
                                color: isSelect == "music" ? AppColors.top_color : AppColors.transparent,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(80)),
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          isSelect = "podcast";
                        });
                      },
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              Strings.podcast,
                              style: TextStyle(
                                  fontSize: SV.setSP(55),
                                  color: isSelect == "podcast" ? Colors.white : AppColors.text_grey_color,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: Constants.fontName),
                            ),
                            Container(
                              width: SV.setWidth(70),
                              child: Divider(
                                thickness: 3,
                                color: isSelect == "podcast" ? AppColors.top_color : AppColors.transparent,
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: SV.setHeight(50)),
                isSelect == "music" ?
                Row(
                  children: [
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          isMusicSelect = "playlist";
                        });
                      },
                      child: Container(
                        height: SV.setHeight(70),
                        width: SV.setWidth(260),
                        decoration: BoxDecoration(
                            color: isMusicSelect == "playlist" ? AppColors.light_blue: AppColors.transparent,
                            borderRadius: BorderRadius.circular(360),
                            border: Border.all(color: AppColors.white,width: 1)
                        ),
                        child:Center(
                          child: Text(
                            Strings.platlist,
                            style: TextStyle(
                                fontSize: SV.setSP(35),
                                color: isMusicSelect == "playlist" ? Colors.white : AppColors.text_grey_color,
                                fontWeight: FontWeight.w500,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setHeight(30)),
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          isMusicSelect = "artists";
                        });
                      },
                      child: Container(
                        height: SV.setHeight(70),
                        width: SV.setWidth(260),
                        decoration: BoxDecoration(
                            color: isMusicSelect == "artists" ? AppColors.light_blue: AppColors.transparent,
                            borderRadius: BorderRadius.circular(360),
                            border: Border.all(color: AppColors.white,width: 1)
                        ),
                        child:Center(
                          child: Text(
                            Strings.artists,
                            style: TextStyle(
                                fontSize: SV.setSP(35),
                                color: isMusicSelect == "artists" ? Colors.white : AppColors.text_grey_color,
                                fontWeight: FontWeight.w500,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setHeight(30)),
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          isMusicSelect = "albums";
                        });
                      },
                      child: Container(
                        height: SV.setHeight(70),
                        width: SV.setWidth(260),
                        decoration: BoxDecoration(
                            color: isMusicSelect == "albums" ? AppColors.light_blue: AppColors.transparent,
                            borderRadius: BorderRadius.circular(360),
                            border: Border.all(color: AppColors.white,width: 1)
                        ),
                        child:Center(
                          child: Text(
                            Strings.albums,
                            style: TextStyle(
                                fontSize: SV.setSP(35),
                                color: isMusicSelect == "albums" ? Colors.white : AppColors.text_grey_color,
                                fontWeight: FontWeight.w500,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                      ),
                    )
                  ],
                ) :
                Row(
                  children: [
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          isPodcastSelect = "episode";
                        });
                      },
                      child: Container(
                        height: SV.setHeight(70),
                        width: SV.setWidth(260),
                        decoration: BoxDecoration(
                            color: isPodcastSelect == "episode" ? AppColors.light_blue: AppColors.transparent,
                            borderRadius: BorderRadius.circular(360),
                            border: Border.all(color: AppColors.white,width: 1)
                        ),
                        child:Center(
                          child: Text(
                            Strings.episode,
                            style: TextStyle(
                                fontSize: SV.setSP(35),
                                color: isPodcastSelect == "episode" ? Colors.white : AppColors.text_grey_color,
                                fontWeight: FontWeight.w500,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setHeight(30)),
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          isPodcastSelect = "download";
                        });
                      },
                      child: Container(
                        height: SV.setHeight(70),
                        width: SV.setWidth(260),
                        decoration: BoxDecoration(
                            color: isPodcastSelect == "download" ? AppColors.light_blue: AppColors.transparent,
                            borderRadius: BorderRadius.circular(360),
                            border: Border.all(color: AppColors.white,width: 1)
                        ),
                        child:Center(
                          child: Text(
                            Strings.downloads,
                            style: TextStyle(
                                fontSize: SV.setSP(35),
                                color: isPodcastSelect == "download" ? Colors.white : AppColors.text_grey_color,
                                fontWeight: FontWeight.w500,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setHeight(30)),
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          isPodcastSelect = "show";
                        });
                      },
                      child: Container(
                        height: SV.setHeight(70),
                        width: SV.setWidth(260),
                        decoration: BoxDecoration(
                            color: isPodcastSelect == "show" ? AppColors.light_blue: AppColors.transparent,
                            borderRadius: BorderRadius.circular(360),
                            border: Border.all(color: AppColors.white,width: 1)
                        ),
                        child:Center(
                          child: Text(
                            Strings.show,
                            style: TextStyle(
                                fontSize: SV.setSP(35),
                                color: isPodcastSelect == "show" ? Colors.white : AppColors.text_grey_color,
                                fontWeight: FontWeight.w500,
                                fontFamily: Constants.fontName),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: SV.setHeight(70)),
          Expanded(
              child: isSelect == "music" ? isMusicSelect == "playlist" || isMusicSelect == "albums" ?
              ListView.builder(
                  padding:
                  EdgeInsets.symmetric(horizontal: SV.setHeight(40)),
                  itemCount: 20,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(bottom:SV.setHeight(50)),
                      child: Row(
                        children: [
                          Container(
                            width: SV.setHeight(115),
                            height: SV.setHeight(115),
                            decoration: BoxDecoration(
                                color: AppColors.blue,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 7.0)
                                ]),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Image.asset(
                                ImagePath.img_temp_2,
                                fit: BoxFit.fill,
                                height: SV.setHeight(115),
                                width: SV.setHeight(115),
                              ),
                            ),
                          ),
                          SizedBox(width: SV.setHeight(35)),
                          Expanded(
                            child: Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Billie Jean',
                                  style: TextStyle(
                                      fontSize: SV.setSP(40),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: Constants.fontName),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(height: SV.setHeight(12)),
                                Text(
                                  'Michael Jackson',
                                  style: TextStyle(
                                      fontSize: SV.setSP(35),
                                      color: AppColors.text_grey_color,
                                      fontFamily: Constants.fontName),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(height: SV.setHeight(12)),
                                Divider(
                                  height: 2,
                                  color: AppColors.text_grey_color,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  }) :
              GridView.builder(
                  padding: EdgeInsets.all(0),
                  itemCount: 12,
                  gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      childAspectRatio: MediaQuery.of(context)
                          .size
                          .width /
                          (MediaQuery.of(context).size.height /
                              1.7)), itemBuilder: (context,index){
                return GestureDetector(
                  onTap: (){
                    Navigator.push<dynamic>(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (BuildContext context) =>
                            ArtistDetailScreen(),
                      ),
                    );
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height:SV.setHeight(230),
                        width:SV.setHeight(230),
                        margin: EdgeInsets.all(SV.setWidth(25)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(360),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 6.0)
                            ]),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(360),
                          child: Image.asset(
                            ImagePath.ic_music_back,
                            fit: BoxFit.fill,
                            height: double.infinity,
                            width: double.infinity,
                          ),
                        ),
                      ),
                      Text(
                        'Arijit Singh',
                        textAlign:TextAlign.center,
                        style: TextStyle(
                            fontSize: SV.setSP(41),
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontFamily: Constants.fontName),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      )
                    ],
                  ),
                );
              }):
              isPodcastSelect == "episode" ? ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: SV.setHeight(40)),
                  itemCount: 15,
                  primary: controller == null,
                  controller: controller,
                  itemBuilder: (context,index){
                    return Container(
                      padding: EdgeInsets.all(SV.setHeight(25)),
                      margin: EdgeInsets.only(bottom:SV.setHeight(50)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                width: SV.setHeight(120),
                                height: SV.setHeight(120),
                                decoration: BoxDecoration(
                                    color: AppColors.blue,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black26,
                                          offset: Offset(0.0, 2.0),
                                          blurRadius: 7.0)
                                    ]),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.asset(
                                    ImagePath.img_temp_2,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              SizedBox(width: SV.setHeight(25)),
                              Expanded(
                                child: Text(
                                  '5 Impactful moment from a Strategy session in Phoenix',
                                  style: TextStyle(
                                      fontSize: SV.setSP(40),
                                      color: AppColors.white,
                                      height: 1.5,
                                      fontFamily: Constants.fontName),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: SV.setHeight(25)),
                          Text(
                            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.',
                            style: TextStyle(
                                fontSize: SV.setSP(35),
                                color: AppColors.text_grey_color,
                                height: 1.5,
                                fontFamily: Constants.fontName),
                          ),
                          SizedBox(height: SV.setHeight(25)),
                          Divider(
                            height: 2,
                            color: AppColors.text_grey_color,
                          ),
                          SizedBox(height: SV.setHeight(25)),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                height: SV.setHeight(70),
                                width: SV.setHeight(70),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: AppColors.play_back_color
                                ),
                                child: Icon(
                                  Icons.play_arrow_rounded,
                                  color: AppColors.white,
                                  size: SV.setHeight(45),
                                ),
                              ),
                              SizedBox(width: SV.setHeight(25)),
                              Expanded(
                                child: Text(
                                  'Today - 48.34',
                                  style: TextStyle(
                                      fontSize: SV.setSP(35),
                                      color: AppColors.white,
                                      fontFamily: Constants.fontName),
                                ),
                              ),
                              Container(
                                height: SV.setHeight(70),
                                width: SV.setHeight(70),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: AppColors.transparent,
                                  border: Border.all(color: AppColors.text_grey_color,width: 1)
                                ),
                                child: Icon(
                                  Icons.download_sharp,
                                  color: AppColors.white,
                                  size: SV.setHeight(35),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    );
              })
                  : isPodcastSelect == "download" ? ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: SV.setHeight(40)),
                  itemCount: 15,
                  primary: controller == null,
                  controller: controller,
                  itemBuilder: (context,index){
                    return Container(
                      padding: EdgeInsets.all(SV.setHeight(25)),
                      margin: EdgeInsets.only(bottom:SV.setHeight(50)),
                      decoration: BoxDecoration(
                        color: AppColors.light_blue,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                width: SV.setHeight(120),
                                height: SV.setHeight(120),
                                decoration: BoxDecoration(
                                    color: AppColors.blue,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black26,
                                          offset: Offset(0.0, 2.0),
                                          blurRadius: 7.0)
                                    ]),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.asset(
                                    ImagePath.img_temp_2,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              SizedBox(width: SV.setHeight(25)),
                              Expanded(
                                child: Text(
                                  '5 Impactful moment from a Strategy session in Phoenix',
                                  style: TextStyle(
                                      fontSize: SV.setSP(40),
                                      color: AppColors.white,
                                      height: 1.5,
                                      fontFamily: Constants.fontName),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: SV.setHeight(25)),
                          Text(
                            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.',
                            style: TextStyle(
                                fontSize: SV.setSP(35),
                                color: AppColors.text_grey_color,
                                height: 1.5,
                                fontFamily: Constants.fontName),
                          ),
                          SizedBox(height: SV.setHeight(25)),
                          Divider(
                            height: 2,
                            color: AppColors.text_grey_color,
                          ),
                          SizedBox(height: SV.setHeight(25)),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                height: SV.setHeight(70),
                                width: SV.setHeight(70),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: AppColors.play_back_color
                                ),
                                child: Icon(
                                  Icons.play_arrow_rounded,
                                  color: AppColors.white,
                                  size: SV.setHeight(45),
                                ),
                              ),
                              SizedBox(width: SV.setHeight(25)),
                              Expanded(
                                child: Text(
                                  'Today - 48.34',
                                  style: TextStyle(
                                      fontSize: SV.setSP(35),
                                      color: AppColors.white,
                                      fontFamily: Constants.fontName),
                                ),
                              ),
                              Container(
                                height: SV.setHeight(70),
                                width: SV.setHeight(70),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  gradient: new LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      AppColors.top_color,
                                      AppColors.bottom_color
                                    ],
                                  ),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Image.asset(ImagePath.ic_checked),
                                ),
                              ),
                              SizedBox(width: SV.setHeight(25)),
                              Container(
                                height: SV.setHeight(70),
                                width: SV.setHeight(70),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  gradient: new LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      AppColors.top_color,
                                      AppColors.bottom_color
                                    ],
                                  ),
                                ),
                                child: Icon(
                                  Icons.download_sharp,
                                  color: AppColors.white,
                                  size: SV.setHeight(35),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  })
                  : ListView.builder(
                  padding:
                  EdgeInsets.symmetric(horizontal: SV.setHeight(40)),
                  itemCount: 20,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(bottom:SV.setHeight(50)),
                      child: Row(
                        children: [
                          Container(
                            width: SV.setHeight(115),
                            height: SV.setHeight(115),
                            decoration: BoxDecoration(
                                color: AppColors.blue,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0.0, 2.0),
                                      blurRadius: 7.0)
                                ]),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Image.asset(
                                ImagePath.img_temp_2,
                                fit: BoxFit.fill,
                                height: SV.setHeight(115),
                                width: SV.setHeight(115),
                              ),
                            ),
                          ),
                          SizedBox(width: SV.setHeight(35)),
                          Expanded(
                            child: Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Dickinson: Season 2',
                                  style: TextStyle(
                                      fontSize: SV.setSP(40),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: Constants.fontName),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(height: SV.setHeight(12)),
                                Text(
                                  'Update yesterday - Joe Budded, Rory,& Mal',
                                  style: TextStyle(
                                      fontSize: SV.setSP(35),
                                      color: AppColors.text_grey_color,
                                      fontFamily: Constants.fontName),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(height: SV.setHeight(12)),
                                Divider(
                                  height: 2,
                                  color: AppColors.text_grey_color,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  })

          )
        ],
      ),
    );
  }


}