
import 'package:flutter/material.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import 'BookingDetailsScreen.dart';

class BookedScreen extends StatefulWidget {

  @override
  _BookedScreenState createState() => _BookedScreenState();
}

class _BookedScreenState extends State<BookedScreen> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: MediaQuery.of(context).padding.top+SV.setWidth(20)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin:EdgeInsets.only(top:7),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.white,
                          size: SV.setHeight(50),
                        ),
                      ),
                    ),
                    SizedBox(width: SV.setWidth(30)),
                    Text(
                      Strings.booked,
                      style: TextStyle(
                          fontSize: SV.setSP(70),
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontFamily: Constants.fontName),
                    ),
                  ],
                ),
              ),
              SizedBox(height: SV.setHeight(50)),
              Expanded(
                child: ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: SV.setHeight(45)),
                    itemCount: 8,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: (){
                          Navigator.push<dynamic>(
                            context,
                            MaterialPageRoute<dynamic>(
                              builder: (BuildContext context) => BookingDetailsScreen(
                                isFrom: "Booked",
                              ),
                            ),
                          );
                        },
                        child: Container(
                          padding: EdgeInsets.all(SV.setHeight(20)),
                          margin: EdgeInsets.only(bottom: SV.setHeight(45)),
                          decoration: BoxDecoration(
                            color: AppColors.light_blue,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Visibility(
                                visible:
                                Constants.userType == "Artist" ? false : true,
                                child: Row(
                                  children: [
                                    Container(
                                      width: SV.setHeight(200),
                                      height: SV.setHeight(240),
                                      decoration: BoxDecoration(
                                          color: AppColors.blue,
                                          borderRadius: BorderRadius.circular(8),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.black26,
                                                offset: Offset(0.0, 2.0),
                                                blurRadius: 7.0)
                                          ]),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(8),
                                        child: Image.asset(
                                          ImagePath.temp_img_profile_1,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: SV.setHeight(20))
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            'Angela Malone',
                                            style: TextStyle(
                                                fontSize: SV.setSP(43),
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: Constants.fontName),
                                          ),
                                        ),
                                        SizedBox(width: SV.setHeight(50)),
                                        Text(
                                          '\$ 350',
                                          style: TextStyle(
                                              fontSize: SV.setSP(43),
                                              color: Colors.white,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: Constants.fontName),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: SV.setHeight(20)),
                                    Row(
                                      children: [
                                        Image.asset(
                                          ImagePath.ic_text,
                                          width: SV.setHeight(45),
                                          height: SV.setHeight(45),
                                        ),
                                        SizedBox(width: SV.setHeight(20)),
                                        Expanded(
                                          child: Text('Concert on available city',
                                              style: TextStyle(
                                                  fontSize: SV.setSP(36),
                                                  color: AppColors.white,
                                                  height: 1.5,
                                                  fontFamily: Constants.fontName)),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: SV.setHeight(15)),
                                    Row(
                                      children: [
                                        Image.asset(
                                          ImagePath.ic_location_concert,
                                          width: SV.setHeight(45),
                                          height: SV.setHeight(45),
                                        ),
                                        SizedBox(width: SV.setHeight(20)),
                                        Expanded(
                                          child: Text('Akshaya Nagar 1st Block',
                                              style: TextStyle(
                                                  fontSize: SV.setSP(36),
                                                  color: AppColors.white,
                                                  height: 1.5,
                                                  fontFamily: Constants.fontName)),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: SV.setHeight(15)),
                                    Row(
                                      children: [
                                        Image.asset(
                                          ImagePath.ic_date_from,
                                          width: SV.setHeight(45),
                                          height: SV.setHeight(45),
                                        ),
                                        SizedBox(width: SV.setHeight(20)),
                                        Expanded(
                                          child: Text('November 10, 2021',
                                              style: TextStyle(
                                                  fontSize: SV.setSP(36),
                                                  color: AppColors.white,
                                                  height: 1.5,
                                                  fontFamily: Constants.fontName)),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );

  }

}