import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/AudioSongPlayScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/screen_size_utils.dart';
import 'package:seekbar/seekbar.dart';

import '../app_theme.dart';

class AlbumSongListScreen extends StatefulWidget {
  @override
  _AlbumSongListScreenState createState() => _AlbumSongListScreenState();
}

class _AlbumSongListScreenState extends State<AlbumSongListScreen> {
  bool isPlay = false;

  double _value = 0.30;
  double _secondValue = 0.0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppColors.back_blue,
          body: Stack(
            children: [
              Column(
                children: [
                  SizedBox(
                      height:
                      MediaQuery.of(context).padding.top + SV.setWidth(20)),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: SV.setWidth(50)),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 6),
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: AppColors.text_grey_color,
                              size: SV.setHeight(45),
                            ),
                          ),
                        ),
                        SizedBox(width: SV.setWidth(45)),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            margin: EdgeInsets.only(top: 6),
                            child: Image.asset(
                              ImagePath.ic_like,
                              width: SV.setHeight(50),
                              height: SV.setHeight(50),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: SV.setWidth(70)),
                  Container(
                    width: SV.setHeight(400),
                    height: SV.setHeight(400),
                    decoration: BoxDecoration(
                        color: AppColors.blue,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black26,
                              offset: Offset(0.0, 2.0),
                              blurRadius: 7.0)
                        ]),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Image.asset(
                        ImagePath.img_temp_2,
                        fit: BoxFit.fill,
                        height: SV.setHeight(400),
                        width: SV.setHeight(400),
                      ),
                    ),
                  ),
                  SizedBox(height: SV.setWidth(55)),
                  Text(
                    'Top Hindi Songs',
                    style: TextStyle(
                        fontSize: SV.setSP(55),
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontFamily: Constants.fontName),
                  ),
                  SizedBox(height: SV.setWidth(100)),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        if (isPlay) {
                          isPlay = false;
                        } else {
                          isPlay = true;
                        }
                      });
                    },
                    child: Container(
                      height: SV.setHeight(90),
                      width: SV.setWidth(210),
                      decoration: BoxDecoration(
                        color: AppTheme.apptheme,
                        borderRadius: BorderRadius.circular(360.0),
                        gradient: new LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [AppColors.top_color, AppColors.bottom_color],
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            offset: Offset(0.0, 2.0),
                            blurRadius: 3.0,
                          )
                        ],
                      ),
                      child: Icon(
                        isPlay == true
                            ? Icons.pause_rounded
                            : Icons.play_arrow_rounded,
                        color: AppColors.white,
                        size: SV.setHeight(75),
                      ),
                    ),
                  ),
                  SizedBox(height: SV.setWidth(60)),
                  Expanded(
                    child: ListView.builder(
                        padding:
                        EdgeInsets.symmetric(horizontal: SV.setHeight(40)),
                        itemCount: 20,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: EdgeInsets.symmetric(
                                vertical: SV.setHeight(35)),
                            child: Row(
                              children: [
                                Container(
                                  width: SV.setHeight(115),
                                  height: SV.setHeight(115),
                                  decoration: BoxDecoration(
                                      color: AppColors.blue,
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0.0, 2.0),
                                            blurRadius: 7.0)
                                      ]),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Image.asset(
                                      ImagePath.img_temp_2,
                                      fit: BoxFit.fill,
                                      height: SV.setHeight(115),
                                      width: SV.setHeight(115),
                                    ),
                                  ),
                                ),
                                SizedBox(width: SV.setHeight(35)),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Billie Jean',
                                        style: TextStyle(
                                            fontSize: SV.setSP(40),
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: Constants.fontName),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      SizedBox(height: SV.setHeight(12)),
                                      Text(
                                        'Michael Jackson',
                                        style: TextStyle(
                                            fontSize: SV.setSP(35),
                                            color: AppColors.text_grey_color,
                                            fontFamily: Constants.fontName),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      SizedBox(height: SV.setHeight(12)),
                                      Divider(
                                        height: 2,
                                        color: AppColors.text_grey_color,
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  ),
                  Visibility(
                    visible: isPlay,
                    child: SizedBox(height: SV.setHeight(200)),
                  )
                ],
              ),
              isPlay ? Positioned(
                bottom: 0,
                width: MediaQuery.of(context).size.width,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push<dynamic>(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (BuildContext context) =>
                            AudioSongPlayScreen()
                      ),
                    );
                  },
                  child: Container(
                    height: SV.setHeight(220),
                    child: Stack(
                      children: [
                        Positioned(
                          top:SV.setHeight(20),
                          width: MediaQuery.of(context).size.width,
                          child: Container(
                            padding: EdgeInsets.all(SV.setHeight(45)),
                            height: SV.setHeight(200),
                            color: AppColors.back_blue,
                            child: Row(
                              children: [
                                Container(
                                  width: SV.setHeight(115),
                                  height: SV.setHeight(115),
                                  decoration: BoxDecoration(
                                      color: AppColors.blue,
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0.0, 2.0),
                                            blurRadius: 7.0)
                                      ]),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Image.asset(
                                      ImagePath.img_temp_2,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                                SizedBox(width: SV.setHeight(35)),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Do you believe in lonelyness',
                                        style: TextStyle(
                                            fontSize: SV.setSP(40),
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: Constants.fontName),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      SizedBox(height: SV.setHeight(12)),
                                      Text(
                                        'Michael Jackson',
                                        style: TextStyle(
                                            fontSize: SV.setSP(35),
                                            color: AppColors.text_grey_color,
                                            fontFamily: Constants.fontName),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(width: SV.setHeight(25)),
                                Container(
                                  child: Image.asset(
                                    ImagePath.ic_like,
                                    width: SV.setHeight(50),
                                    height: SV.setHeight(50),
                                  ),
                                ),
                                SizedBox(width: SV.setHeight(20)),
                                GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      if (isPlay) {
                                        isPlay = false;
                                      } else {
                                        isPlay = true;
                                      }
                                    });
                                  },
                                  child: Container(
                                    child: Icon(
                                      isPlay == true
                                          ? Icons.pause_rounded
                                          : Icons.play_arrow_rounded,
                                      color: AppColors.text_grey_color,
                                      size: SV.setHeight(65),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        SeekBar(
                          value: _value,
                          secondValue: _secondValue,
                          progressColor: AppTheme.apptheme,
                          secondProgressColor: Colors.blue.withOpacity(0.5),
                          progressWidth: 3,
                          thumbRadius: 10,
                          thumbColor: AppTheme.apptheme,
                          onStartTrackingTouch: () {
                            print('onStartTrackingTouch');
                          },
                          onProgressChanged: (value) {
                            print('onProgressChanged:$value');
                            _value = value;
                          },
                          onStopTrackingTouch: () {
                            print('onStopTrackingTouch');
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ) : Container()
            ],
          ),
        ));
  }
}
