import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import 'LoginScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  //Utils utils;
  //SharedPref _sharedPref;

  @override
  void initState() {
    super.initState();
    //utils = Utils(context: context);
    //_sharedPref = SharedPref();
    new Future.delayed(new Duration(seconds: 2), () {
      readData();
    });
  }

  Future<void> readData() async {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => LoginScreen()),
            (Route<dynamic> route) => false);
  }

  /*@override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }*/

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);

    ScreenUtil.instance.init(context);

    return MaterialApp(
      home: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          body: Stack(
            children: [
              Image.asset(
                ImagePath.splash_bg,
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity,
              ),
              Positioned(
                right: 0,
                child: Image.asset(
                  ImagePath.top_corner,
                  width: SV.setHeight(250),
                  height: SV.setHeight(250),
                ),
              ),
              Positioned(
                left: 0,
                bottom:0,
                child: Image.asset(
                  ImagePath.bottom_corner,
                  height: SV.setHeight(250),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Center(
                    child: Column(
                      children: [
                        Image.asset(
                          ImagePath.splash_app_logo,
                          width: SV.setHeight(350),
                          height: SV.setHeight(350),
                        ),
                        SizedBox(height: SV.setHeight(20)),
                        Text(
                          Strings.app_name.toUpperCase(),
                          style: TextStyle(
                              fontSize: SV.setSP(66),
                              color: AppColors.white,
                              fontWeight: FontWeight.w600,
                              fontFamily: Constants.fontName),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
