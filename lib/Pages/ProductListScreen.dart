
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geeq_music/Pages/ProductDetailScreen.dart';
import 'package:geeq_music/common/AppColors.dart';
import 'package:geeq_music/common/Constants.dart';
import 'package:geeq_music/common/ImagePath.dart';
import 'package:geeq_music/common/Strings.dart';
import 'package:geeq_music/common/screen_size_utils.dart';

import '../app_theme.dart';

class ProductListScreen extends StatefulWidget {
  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: AppColors.back_blue,
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: SV.setHeight(40)),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal:SV.setHeight(50)),
              decoration: BoxDecoration(
                color: AppColors.light_blue,
                borderRadius: BorderRadius.circular(SV.setHeight(360)),
              ),
              child: TextField(
                style: TextStyle(
                    fontSize: SV.setSP(45),
                    color: AppTheme.white,
                    fontWeight: FontWeight.w500,
                    fontFamily: AppTheme.fontName),
                cursorColor: AppTheme.apptheme,
                onChanged: (val) {

                },
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 15),
                    border: InputBorder.none,
                    hintText: Strings.search,
                    hintStyle: TextStyle(
                        fontSize: SV.setSP(45),
                        color: AppColors.grey_light,
                        fontWeight: FontWeight.w500,
                        fontFamily: AppTheme.fontName),
                    prefixIcon: Icon(
                      Icons.search,
                      color: AppColors.grey_light,
                    )),
              ),
            ),
          ),
          SizedBox(height: SV.setHeight(45)),
          Expanded(
            child: GridView.builder(
                padding: EdgeInsets.all(0),
                itemCount: 12,
                gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: MediaQuery.of(context)
                        .size
                        .width /
                        (MediaQuery.of(context).size.height /
                            1.7)), itemBuilder: (context,index){
              return GestureDetector(
                onTap: (){
                  Navigator.push<dynamic>(
                    context,
                    MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) =>
                          ProductDetailScreen(),
                    ),
                  );
                },
                child: Container(
                  margin: EdgeInsets.all(SV.setWidth(25)),
                  child: Column(
                    children: [
                      Container(
                        height: SV.setHeight(370),
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                                topRight: Radius.circular(10)
                            ),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 6.0)
                            ]),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: Image.asset(
                            ImagePath.temp_img_product,
                            fit: BoxFit.fill,
                            height: double.infinity,
                          ),
                        ),
                      ),
                      Container(
                        height: SV.setHeight(130),
                        padding: EdgeInsets.all(SV.setHeight(20)),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(10),
                                bottomLeft: Radius.circular(10)
                            ),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  offset: Offset(0.0, 2.0),
                                  blurRadius: 6.0)
                            ]),
                        child: Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Lather Jackets",
                                    style: TextStyle(
                                        fontSize: SV.setSP(35),
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: Constants.fontName),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  SizedBox(height: SV.setHeight(8)),
                                  Text(
                                    "Black Solid",
                                    style: TextStyle(
                                        fontSize: SV.setSP(25),
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: Constants.fontName),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              ),
                            ),
                            SizedBox(width: SV.setHeight(20)),
                            Container(
                              height: SV.setHeight(50),
                              decoration: BoxDecoration(
                                color: AppColors.dark_blue,
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0.0, 2.0),
                                    blurRadius: 3.0,
                                  )
                                ],
                              ),
                              child: Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal:SV.setWidth(15)),
                                  child: Text(
                                      '\$ 335',
                                      style: TextStyle(
                                          fontSize: SV.setSP(26),
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: Constants.fontName)),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            }),
          )
        ],
      ),
    );

  }


}